using System;
using System.Threading.Tasks;
using Cloudburst.EventBus.Abstractions;
using MassTransit;

namespace Cloudburst.Test.Testing.EventBus {
	public interface TestEvent : IEventMessage {
		string MyName { get; set; }
	}

	public class TestEventConsumer : IConsumer<TestEvent> {
		public async Task Consume(ConsumeContext<TestEvent> context) {
			Console.WriteLine($"Show me the stuff: {context.Message.MyName} [{context.Message.EventId}] {context.Message.Timestamp}");
		}
	}
}
