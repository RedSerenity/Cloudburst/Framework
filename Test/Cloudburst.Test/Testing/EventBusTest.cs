using System;
using System.Threading;
using System.Threading.Tasks;
using Cloudburst.Test.Testing.EventBus;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Cloudburst.Test.Testing {
	public class EventBusTest : BackgroundService {
		private readonly IServiceProvider _serviceProvider;

		public EventBusTest(IServiceProvider serviceProvider) {
			_serviceProvider = serviceProvider;
		}

		protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
			Console.WriteLine("EventBusTest Starting");

			await Task.Delay(5000);

			using (var scope = _serviceProvider.CreateScope()) {
				var publishEndpoint = scope.ServiceProvider.GetService<IPublishEndpoint>();

				await publishEndpoint.Publish<TestEvent>(new {
					MyName = "Tyler", EventId = Guid.NewGuid(), Timestamp = DateTime.UtcNow
				}, stoppingToken);
			}
		}

	}
}
