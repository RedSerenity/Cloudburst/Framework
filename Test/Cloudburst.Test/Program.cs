using System;
using Cloudburst.Core;
using Cloudburst.EventBus.Extensions;
using Cloudburst.Extensions.Core;
using Cloudburst.Version.Abstractions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Cloudburst.Test {
	public class Program {
		public static void Main(string[] args) => CreateHostBuilder(args);
		public static void CreateHostBuilder(string[] args) => Cloud.Downpour<Startup, ServiceVersion>(args, _webHostBuilder, _hostBuilder);

		private static readonly Action<IWebHostBuilder> _webHostBuilder = webBuilder => {
			webBuilder.ConfigureKestrel(options => {
				options.ConfigureNonTlsPorts(5554, 5555);
			});
		};

		private static readonly Action<IHostBuilder> _hostBuilder = hostBuilder => {
			hostBuilder.UseCloudEventBusWithRabbitMq();
		};
	}

	public class ServiceVersion : AssemblyVersion { }
}
