using System;
using System.Collections.Generic;
using System.Net.Http;
using Cloudburst.Consul.HealthChecks;
using Cloudburst.Consul.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Consul {
	public static class HealthChecksBuilderExtensions {
		public static IHealthChecksBuilder AddCloudConsulHealthCheck(this IHealthChecksBuilder builder) {
			return builder.AddCloudConsulHealthCheck(setup => { });
		}

		public static IHealthChecksBuilder AddCloudConsulHealthCheck(this IHealthChecksBuilder builder, IEnumerable<string> tags) {
			return builder.AddCloudConsulHealthCheck(setup => { }, tags: tags);
		}

		public static IHealthChecksBuilder AddCloudConsulHealthCheck(this IHealthChecksBuilder builder, IEnumerable<string> tags, TimeSpan? timeout) {
			return builder.AddCloudConsulHealthCheck(setup => { }, tags: tags, timeout: timeout);
		}

		public static IHealthChecksBuilder AddCloudConsulHealthCheck(this IHealthChecksBuilder builder, Action<ConsulOptions> setup, string name = null, HealthStatus? failureStatus = default, IEnumerable<string> tags = default, TimeSpan? timeout = default) {
			builder.Services.AddHttpClient();

			var registrationName = name ?? "Cloudburst.Consul.HealthCheck";
			return builder.Add(new HealthCheckRegistration(
				registrationName,
				sp => CreateHealthCheck(sp, registrationName, setup),
				failureStatus,
				tags,
				timeout));
		}

		private static ConsulProviderHealthCheck CreateHealthCheck(IServiceProvider serviceProvider, string name, Action<ConsulOptions> setup) {
			var options = serviceProvider.GetService<ConsulOptions>() ?? new ConsulOptions();

			setup?.Invoke(options);

			var httpClientFactory = serviceProvider.GetRequiredService<IHttpClientFactory>();
			return new ConsulProviderHealthCheck(options, () => httpClientFactory.CreateClient(name));
		}
	}
}
