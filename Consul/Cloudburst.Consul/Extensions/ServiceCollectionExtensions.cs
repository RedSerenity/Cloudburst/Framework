﻿using System;
using Cloudburst.Configuration.Extensions.Options;
using Cloudburst.Consul.Models;
using Consul;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Consul {
	public static class ServiceCollectionExtensions {
		public static IServiceCollection AddCloudConsul(this IServiceCollection services) {
			return services.AddCloudConsul(options => { });
		}

		public static IServiceCollection AddCloudConsul(this IServiceCollection services, Action<ConsulOptions> options) {
			services.TryAddSingleton<IConsulClient>(sp => {
				var consulOptions = sp.GetService<ConsulOptions>() ?? new ConsulOptions();

				options?.Invoke(consulOptions);

				return new ConsulClient(config => {
					config.Address = new Uri(consulOptions.Address);
					config.Datacenter = consulOptions.DataCenter;
					config.Token = consulOptions.Token;
					config.WaitTime = TimeSpan.FromSeconds(consulOptions.WaitTime);
				});
			});

			return services;
		}

		public static IServiceCollection AddCloudConsulOptions(this IServiceCollection services, IConfiguration configuration) {
			services.BindOption<ConsulOptions>(configuration);
			return services;
		}
	}
}
