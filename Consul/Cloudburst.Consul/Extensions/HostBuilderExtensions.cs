using System;
using Cloudburst.Consul.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Consul {
	public static class HostBuilderExtensions {
		public static IHostBuilder UseCloudConsul(this IHostBuilder builder) {
			return builder.UseCloudConsul(options => { });
		}

		public static IHostBuilder UseCloudConsul(this IHostBuilder builder, Action<ConsulOptions> options) {
			builder.ConfigureServices((context, services) => {
				services
					.AddCloudConsulOptions(context.Configuration)
					.AddCloudConsul(options)
					.AddHealthChecks()
						.AddCloudConsulHealthCheck(new [] { "ready" });
			});

			return builder;
		}
	}
}
