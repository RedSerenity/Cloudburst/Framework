using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Cloudburst.Consul.Models;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;

namespace Cloudburst.Consul.HealthChecks {
	public class ConsulProviderHealthCheck : IHealthCheck {
		private readonly ConsulOptions _options;
		private readonly Func<HttpClient> _httpClientFactory;

		public ConsulProviderHealthCheck(ConsulOptions options, Func<HttpClient> httpClientFactory) {
			_options = options ?? throw new ArgumentNullException(nameof(options));
			_httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
		}

		public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken()) {
			try {
				var client = _httpClientFactory();
				var uri = new Uri($"{_options.Address}/v1/status/leader");

				var result = await client.GetAsync(uri, cancellationToken);

				return result.IsSuccessStatusCode ?
					HealthCheckResult.Healthy() :
					new HealthCheckResult(context.Registration.FailureStatus, $"Consul response was {result.StatusCode}");
			} catch (Exception exception) {
				return new HealthCheckResult(context.Registration.FailureStatus, exception: exception);
			}
		}
	}
}
