﻿using System;
using Cloudburst.Configuration.Attributes;

namespace Cloudburst.Consul.Models {
	[ConfigurationKey("Cloudburst:Consul")]
	public class ConsulOptions {
		public string Address { get; set; } = "http://localhost:8500";
		public string DataCenter { get; set; } = "dc1";
		public int WaitTime { get; set; } = 2;
		public string Token { get; set; }
	}
}
