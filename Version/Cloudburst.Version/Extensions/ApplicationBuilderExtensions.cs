using System;
using Cloudburst.Version;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Version {
	public static class ApplicationBuilderExtensions {
		public static IApplicationBuilder UseCloudVersioning(this IApplicationBuilder app) {
			return app.MapWhen(
				IsVersionEndpoint,
				builder => builder.UseMiddleware<VersionMiddleware>()
			);
		}

		private static bool IsVersionEndpoint(HttpContext httpContext) {
			return httpContext.Request.Method == "GET" &&
						httpContext.Request.Path.StartsWithSegments("/version", out PathString remaining) &&
						(String.IsNullOrEmpty(remaining) || String.Equals("/", remaining));
		}
	}
}
