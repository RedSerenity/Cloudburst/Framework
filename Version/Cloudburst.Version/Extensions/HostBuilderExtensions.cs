using System;
using Cloudburst.Version.Abstractions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Version {
	public static class HostBuilderExtensions {
		public static IHostBuilder UseCloudVersioning<TServiceVersion>(this IHostBuilder builder) where TServiceVersion : class, IServiceVersion {
			return builder.ConfigureServices((builderContext, services) => {
				services.TryAddSingleton<IServiceVersion, TServiceVersion>();

				try {
					var instance = (IServiceVersion) Activator.CreateInstance(typeof(TServiceVersion));

					if (instance is null) {
						throw new NullReferenceException($"Unable to create instance of {typeof(TServiceVersion).FullName}");
					}

					Console.WriteLine($"Application Version: {instance}");
					Console.WriteLine($"Application Version (Full): {instance.FullVersion()}");
				} catch (Exception exception) {
					Console.WriteLine($"Unable to get Application Version: {exception.Message}");
				}
			});
		}
	}
}
