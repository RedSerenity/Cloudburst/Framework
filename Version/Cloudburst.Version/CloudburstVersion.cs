using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Cloudburst.Version.Abstractions;

namespace Cloudburst.Version {
	public class CloudburstVersion {
		public static IServiceVersion GetVersion() {
			Type versionInterface = typeof(IServiceVersion);
			List<IServiceVersion> types = AppDomain.CurrentDomain
				.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(p => versionInterface.IsAssignableFrom(p) && !p.IsInterface && !p.IsAbstract)
				.Select(s => (IServiceVersion) Activator.CreateInstance(s))
				.ToList();

			if (!types.Any()) {
				return null;
			}

			if (types.Count > 1) {
				throw new AmbiguousMatchException("Multiple implementations of IServiceVersion found. You should only have 1 implementation per service.");
			}

			return types.First();
		}
	}
}
