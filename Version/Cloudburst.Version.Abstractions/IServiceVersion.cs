using System;

namespace Cloudburst.Version.Abstractions {
	public interface IServiceVersion {
		int Major { get; }
		int Minor { get; }
		int Patch { get; }
		string PreRelease { get; }
		int Build { get; }
		Guid UniqueInstanceIdentifier { get; }

		string FullVersion(); // Includes PreRelease and Path
		string ToString(); // Only Includes Major.Minor.Patch
	}
}
