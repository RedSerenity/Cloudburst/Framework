using Microsoft.Extensions.Hosting;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Swagger {
	public static class HostBuilderExtensions {
		public static IHostBuilder UseCloudSwagger(this IHostBuilder builder) {
			builder.ConfigureServices((context, services) => {
				services.AddCloudSwagger();
			});

			return builder;
		}
	}
}
