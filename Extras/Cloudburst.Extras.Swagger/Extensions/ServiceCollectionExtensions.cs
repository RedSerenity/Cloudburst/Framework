﻿using System;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

// ReSharper disable AssignNullToNotNullAttribute
// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Swagger {
	public static class ServiceCollectionExtensions {
		public static IServiceCollection AddCloudSwagger(this IServiceCollection services) {
			services.AddSwaggerGen(options => {
				var entryAssembly = Assembly.GetEntryAssembly() ?? throw new ArgumentNullException($"Swagger Entry Assembly");

				var assemblyVersion = entryAssembly.GetName().Version ?? new Version(0,0,0,0);
				var version = $"v{assemblyVersion.Major}.{assemblyVersion.Minor}.{assemblyVersion.Revision}";

				var title = entryAssembly.GetCustomAttribute<AssemblyTitleAttribute>()?.Title ?? "(Name Not Set)";
				var description = entryAssembly.GetCustomAttribute<AssemblyDescriptionAttribute>()?.Description  ?? "(Description Not Set)";

				options.SwaggerDoc(version, new OpenApiInfo {
					Title = title,
					Version = version,
					Description = description
				});
			});

			return services;
		}
	}
}
