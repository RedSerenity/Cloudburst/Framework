using System;
using System.Reflection;
using Microsoft.AspNetCore.Builder;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Swagger {
	public static class ApplicationBuilderExtensions {
		public static IApplicationBuilder UseCloudSwagger(this IApplicationBuilder app) {
			app.UseSwagger();
			app.UseSwaggerUI(c => {
				var entryAssembly = Assembly.GetEntryAssembly() ?? throw new ArgumentNullException($"Swagger Entry Assembly");

				var assemblyVersion = entryAssembly.GetName().Version ?? new Version(0,0,0,0);

				var version = $"v{assemblyVersion.Major}.{assemblyVersion.Minor}.{assemblyVersion.Revision}";

				var title = entryAssembly.GetCustomAttribute<AssemblyTitleAttribute>()?.Title ?? "(Name Not Set)";

				c.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"{title} {version}");
			});

			return app;
		}
	}
}
