﻿using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.StaticFiles;

/*
   	To use this you need to add the following to your .csproj file that contains your .proto files

   	<PropertyGroup>
   		<GenerateEmbeddedFilesManifest>true</GenerateEmbeddedFilesManifest>
   	</PropertyGroup>

   	<ItemGroup>
   		<EmbeddedResource Include="*.proto" />
   	</ItemGroup>
 */

namespace Cloudburst.Extras.ProtoMiddleware.Extensions {
	public static class ApplicationBuilderExtensions {
		public static IApplicationBuilder UseProtoMiddleware(this IApplicationBuilder app, params Assembly[] assemblies) {
			var protoFileProvider = new FileExtensionContentTypeProvider();
			protoFileProvider.Mappings.Clear();
			protoFileProvider.Mappings.Add(".proto", "text/plain");

			var assemblyFileProvider = new AssemblyFileProvider(assemblies);

			app.UseStaticFiles(new StaticFileOptions {
				FileProvider = assemblyFileProvider,
				RequestPath = "/proto",
				ContentTypeProvider = protoFileProvider
			});

			app.UseDirectoryBrowser(new DirectoryBrowserOptions {
				FileProvider = assemblyFileProvider,
				RequestPath = "/proto",
				Formatter = new CloudburstDirectoryFormatter()
			});

			return app;
		}
	}
}
