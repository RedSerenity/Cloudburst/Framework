using System;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.FileProviders;

namespace Cloudburst.Extras.ProtoMiddleware.Extensions {
	public static class AssemblyExtensions {
		public static IFileProvider GetFileProvider(this Assembly assembly) {
			IFileProvider fileProvider;
			try {
				fileProvider = new ManifestEmbeddedFileProvider(assembly);
			} catch (InvalidOperationException) {
				fileProvider = new EmbeddedFileProvider(assembly);
			}

			return fileProvider;
		}

		public static string Name(this Assembly assembly) {
			return (string) assembly.GetName().Name;
		}

		public static DateTimeOffset GetLastModified(this Assembly assembly) {
			var lastModified = DateTimeOffset.UtcNow;
			if (!String.IsNullOrEmpty(assembly.Location)) {
				try {
					lastModified = File.GetLastWriteTimeUtc(assembly.Location);
				} catch (PathTooLongException) { } catch (UnauthorizedAccessException) { }
			}

			return lastModified;
		}
	}
}
