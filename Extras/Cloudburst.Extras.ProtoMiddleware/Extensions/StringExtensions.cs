using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Cloudburst.Extras.ProtoMiddleware.Extensions {
	public static class StringExtensions {
		public static (string, string) ExtractAssemblyFromPath(this string path) {
			MatchCollection matches = Regex.Matches(path, "^\\/([^\\/]*)(.*)");
			if (matches.Count == 0) {
				throw new Exception($"Regex.Match: Path doesn't exist {path}");
			}

			var match = matches.First();
			var assemblyName = match.Groups[1].Value;
			var newPath = match.Groups[2].Value;

			return (assemblyName, newPath);
		}
	}
}
