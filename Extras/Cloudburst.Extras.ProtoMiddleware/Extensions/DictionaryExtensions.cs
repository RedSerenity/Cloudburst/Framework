using System.Collections.Generic;
using Microsoft.Extensions.FileProviders;

namespace Cloudburst.Extras.ProtoMiddleware.Extensions {
	public static class DictionaryExtensions {
		public static IFileProvider GetProvider(this IDictionary<string, IFileProvider> dict, string name) {
			return dict[name];
		}
	}
}
