using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.FileProviders;

namespace Cloudburst.Extras.ProtoMiddleware.Extensions {
	public static class FileInfoExtensions {
		public static IList<IFileInfo> FilterOutManifest(this IEnumerable<IFileInfo> list) {
			return list
				.Where(directory => directory.Name != "Microsoft.Extensions.FileProviders.Embedded.Manifest.xml")
				.ToList();
		}
	}
}
