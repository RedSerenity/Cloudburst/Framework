using System;
using System.Collections.Generic;
using System.Reflection;
using Cloudburst.Extras.ProtoMiddleware.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;

namespace Cloudburst.Extras.ProtoMiddleware {
	public class AssemblyFileProvider : IFileProvider {
		private readonly IList<AssemblyAsDirectory> _providerDirectories;
		private readonly IDictionary<string, IFileProvider> _providers;

		public AssemblyFileProvider(params Assembly[] assemblies) {
			_providers = new Dictionary<string, IFileProvider>();
			_providerDirectories = new List<AssemblyAsDirectory>();

			foreach (var assembly in assemblies) {
				string assemblyName = assembly.Name();
				var fileProvider = assembly.GetFileProvider();

				_providers.Add(assemblyName, fileProvider);
				_providerDirectories.Add(new AssemblyAsDirectory(assemblyName, assembly.GetLastModified()));
			}
		}

		public IFileInfo GetFileInfo(string subPath) {
			Console.WriteLine($"Requesting Path: {subPath}");
			if (String.IsNullOrEmpty(subPath) || subPath == "/") {
				return new NotFoundFileInfo(subPath);
			}

			var path = subPath.ExtractAssemblyFromPath();
			var provider = _providers.GetProvider(path.Item1);
			Console.WriteLine($"Provider: {path.Item1}");
			Console.WriteLine($"New Path: {path.Item2}");
			return provider.GetFileInfo(path.Item2);
		}

		public IDirectoryContents GetDirectoryContents(string subPath) {
			Console.WriteLine($"Requesting Path: {subPath}");
			if (String.IsNullOrEmpty(subPath) || subPath == "/") {
				Console.WriteLine("Providing Contents from ProviderDirectories");
				return new AssemblyDirectoryContents(_providerDirectories);
			}

			var path = subPath.ExtractAssemblyFromPath();
			var provider = _providers.GetProvider(path.Item1);
			var results = provider.GetDirectoryContents(path.Item2);
			var filteredResults = results.FilterOutManifest();

			Console.WriteLine($"Provider: {path.Item1}");
			Console.WriteLine($"New Path: {path.Item2}");

			return new AssemblyDirectoryContents(filteredResults);
		}

		public IChangeToken Watch(string filter) => NullChangeToken.Singleton;
	}
}
