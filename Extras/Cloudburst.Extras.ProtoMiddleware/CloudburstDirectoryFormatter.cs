using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.FileProviders;

namespace Cloudburst.Extras.ProtoMiddleware {
	public class CloudburstDirectoryFormatter : IDirectoryFormatter {
		// Mostly taken from HtmlDirectoryFormatter.cs in .NET Core.
		// TODO: Redo this whole thing.
		private const string TextHtmlUtf8 = "text/html; charset=utf-8";

		/// <summary>
		/// Constructs the <see cref="HtmlDirectoryFormatter"/>.
		/// </summary>
		public CloudburstDirectoryFormatter() { }

		/// <summary>
		/// Generates an HTML view for a directory.
		/// </summary>
		public virtual Task GenerateContentAsync(HttpContext context, IEnumerable<IFileInfo> contents) {
			if (context == null) {
				throw new ArgumentNullException(nameof(context));
			}

			if (contents == null) {
				throw new ArgumentNullException(nameof(contents));
			}

			context.Response.ContentType = TextHtmlUtf8;

			if (HttpMethods.IsHead(context.Request.Method)) {
				// HEAD, no response body
				return Task.CompletedTask;
			}

			PathString requestPath = context.Request.PathBase + context.Request.Path;

			var builder = new StringBuilder();

			// https://codepen.io/redserenity/pen/oNxOjbK
			builder.AppendFormat(@"
<!DOCTYPE html>
<html lang=""{0}"">
<head>
	<title>Index of: {1}</title>
",
				CultureInfo.CurrentUICulture.TwoLetterISOLanguageName,
				requestPath.Value);

			builder.Append(@"
	<style>
		* {
		  margin: 0;
		  padding: 0;
		}
		html, body {
		  width: 100%;
		  display: flex;
		}
		body {
		  justify-content: center;
		  padding: 60px;
		  background-color: #eeeeee;
		  font-family: ""Segoe UI"", ""Segoe WP"", ""Helvetica Neue"", 'RobotoRegular', sans-serif;
		}
		main {
		  display: flex;
		  flex-direction: column;
		  width: 80%;
		  min-height: 40%;
		  background-color: #ffffff;
		  border: 1px solid #bbbbbb;
		  border-radius: 0.5rem;
		}
		main header {
		  display: flex;
		  flex-direction: column;
		  height: 100px;
		  color: #ffffff;
		  background-color: #333333;
		  border-top-left-radius: 0.5rem;
		  border-top-right-radius: 0.5rem;
		}
		main header a {
		  color: #ffffff;
		  text-decoration: none;
		}
		main header h1, main header h2 {
		  display: flex;
		  align-items: center;
		  flex: auto;
		  padding: 0 1rem;
		  font-size: 18px;
		}
		main header h1 {
		  font-weight: normal;
		  border-bottom: 1px solid #4d4d4d;
		}
		main .file-columns div {
		  flex-basis: 25%;
		}
		main .file-columns div:first-child {
		  flex-basis: 50%;
		}
		main .file-list {
		  list-style-type: none;
		  height: 100%;
		}
		main .file-list li {
		  display: flex;
		}
		main .file-list li a {
		  display: flex;
		  width: 100%;
		  padding: 1rem 0;
		  color: #333333;
		  text-decoration: none;
		}
		main .file-list li a div:first-child {
		  padding-left: 1rem;
		}
		main .file-list li a div:last-child {
		  padding-right: 1rem;
		}
		main .file-list li:hover {
		  background-color: #dddddd;
		}
		main .file-list li div {
		  color: #333333;
		}
  </style>
</head>
<body>
	<main>
		<header>
			<h1><a href=""/"">/</a>");

			string cumulativePath = "/";
			foreach (var segment in requestPath.Value.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries)) {
				cumulativePath = cumulativePath + segment + "/";
				builder.AppendFormat(@"<a href=""{0}"">{1}/</a>", cumulativePath, segment);
			}

			builder.Append(@"
			</h1>
			<h2 class=""file-columns"">
				<div>Name</div>
				<div>Size</div>
				<div>Last Modified</div>
			</h2>
		</header>
		<ul class=""file-list"">
			");

			foreach (var subdir in contents.Where(info => info.IsDirectory)) {
				// Collect directory metadata in a try...catch in case the file is deleted while we're getting the data.
				// The metadata is retrieved prior to calling AppendFormat so if it throws, we won't have written a row
				// to the table.
				try {
					builder.AppendFormat(@"
			<li>
				<a href=""./{0}/"" class=""file-columns"">
					<div>{0}/</div>
					<div>-</div>
					<div>{1}</div>
				</a>
			</li>
			", subdir.Name, subdir.LastModified.ToString("MM/dd/yyyy hh:mm:ss tt", CultureInfo.CurrentCulture));
				} catch (DirectoryNotFoundException) {
					// The physical DirectoryInfo class doesn't appear to throw for either
					// of Name or LastWriteTimeUtc (which backs LastModified in the physical provider)
					// if the directory doesn't exist. However, we don't know what other providers might do.

					// Just skip this directory. It was deleted while we were enumerating.
				} catch (FileNotFoundException) {
					// The physical DirectoryInfo class doesn't appear to throw for either
					// of Name or LastWriteTimeUtc (which backs LastModified in the physical provider)
					// if the directory doesn't exist. However, we don't know what other providers might do.

					// Just skip this directory. It was deleted while we were enumerating.
				}
			}

			foreach (var file in contents.Where(info => !info.IsDirectory)) {
				// Collect file metadata in a try...catch in case the file is deleted while we're getting the data.
				// The metadata is retrieved prior to calling AppendFormat so if it throws, we won't have written a row
				// to the table.
				try {
					builder.AppendFormat(@"
			<li>
				<a href=""./{0}"" class=""file-columns"">
					<div>{0}</div>
					<div>{1} Bytes</div>
					<div>{2}</div>
				</a>
			</li>",
						file.Name,
						file.Length.ToString("n0", CultureInfo.CurrentCulture),
						file.LastModified.ToString(CultureInfo.CurrentCulture));
				} catch (DirectoryNotFoundException) {
					// There doesn't appear to be a case where DirectoryNotFound is thrown in the physical provider,
					// but we don't know what other providers might do.

					// Just skip this file. It was deleted while we were enumerating.
				} catch (FileNotFoundException) {
					// Just skip this file. It was deleted while we were enumerating.
				}
			}

			builder.Append(@"
	  </ul>
	</main>
</body>
</html>
			");
			string data = builder.ToString();
			byte[] bytes = Encoding.UTF8.GetBytes(data);
			context.Response.ContentLength = bytes.Length;
			return context.Response.Body.WriteAsync(bytes, 0, bytes.Length);
		}
	}
}
