using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Extensions.FileProviders;

namespace Cloudburst.Extras.ProtoMiddleware {
	public class AssemblyDirectoryContents : IDirectoryContents {
		private readonly IEnumerable<IFileInfo> _entries;

		public AssemblyDirectoryContents(IEnumerable<IFileInfo> entries) =>
			_entries = entries ?? throw new ArgumentNullException(nameof(entries));

		public bool Exists => true;

		public IEnumerator<IFileInfo> GetEnumerator() => _entries.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => _entries.GetEnumerator();
	}
}
