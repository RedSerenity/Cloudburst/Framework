using System;
using System.IO;
using Microsoft.Extensions.FileProviders;

namespace Cloudburst.Extras.ProtoMiddleware {
	public class AssemblyAsDirectory : IFileInfo {
		public AssemblyAsDirectory(string name, DateTimeOffset lastModified) {
			Name = name;
			LastModified = lastModified;
		}

		public string Name { get; }
		public DateTimeOffset LastModified { get; }

		public Stream CreateReadStream() {
			throw new NotImplementedException();
		}

		public bool Exists => true;
		public long Length => 0;
		public string PhysicalPath => null;
		public bool IsDirectory => true;
	}
}
