namespace Cloudburst.ServiceRegistration.Models {
	public class ServiceCheckOptions {
		public int DeregisterCriticalServiceAfter { get; set; } = 60;
		public int Timeout { get; set; } = 3;
		public int Interval { get; set; } = 30;
		public bool TlsVerify { get; set; } = true;
		public bool AppendGuid { get; set; } = true;
		public string OverrideAddress { get; set; }
		public string OverridePath { get; set; }
	}
}
