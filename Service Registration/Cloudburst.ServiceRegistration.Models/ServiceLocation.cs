namespace Cloudburst.ServiceRegistration.Models {
	public class ServiceLocation {
		public string Address { get; set; }
		public int Port { get; set; }
	}
}
