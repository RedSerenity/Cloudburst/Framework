﻿using System.ComponentModel.DataAnnotations;
using Cloudburst.Configuration.Attributes;

// TODO: Add full support GRPC, multiple services, extended config, etc.

namespace Cloudburst.ServiceRegistration.Models {
	[ConfigurationKey("Cloudburst:ServiceDiscovery")]
	public class ServiceDiscoveryOptions {
		public const int DEFAULT_SERVICE_REGISTRATION = 60;

		[Required]
		public string ServiceName { get; set; }
		public string[] ServiceTags { get; set; }
		public string ServiceAddress { get; set; }
		public int ServicePort { get; set; } = 0;
		public ServiceCheckOptions Check { get; set; }
		public int UpdateFrequency { get; set; } = DEFAULT_SERVICE_REGISTRATION; // Honors Real-time updates

		public ServiceDiscoveryOptions() {
			Check = new ServiceCheckOptions();
		}
	}
}
