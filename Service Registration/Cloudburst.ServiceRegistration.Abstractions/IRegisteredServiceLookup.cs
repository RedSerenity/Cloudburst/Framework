using System.Collections.Generic;
using System.Threading.Tasks;
using Cloudburst.ServiceRegistration.Models;

namespace Cloudburst.ServiceRegistration.Abstractions {
	public interface IRegisteredServiceLookup {
		Task<IEnumerable<ServiceLocation>> GetServices();
		Task<IEnumerable<ServiceLocation>> GetServices(string[] tags);
		Task<ServiceLocation> GetService(string[] tags);
	}
}
