using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cloudburst.Extensions.ServiceRegistration;
using Cloudburst.ServiceRegistration.Abstractions;
using Cloudburst.ServiceRegistration.Models;
using Consul;
using Microsoft.Extensions.Logging;

namespace Cloudburst.ServiceRegistration {
	public class RegisteredServiceLookup : IRegisteredServiceLookup {
		private readonly ILogger<RegisteredServiceLookup> _logger;
		private readonly IConsulClient _consulClient;

		public RegisteredServiceLookup(ILogger<RegisteredServiceLookup> logger, IConsulClient consulClient) {
			_logger = logger;
			_consulClient = consulClient;
		}

		public async Task<IEnumerable<ServiceLocation>> GetServices() {
			return await GetServices(new string[] { });
		}

		public async Task<IEnumerable<ServiceLocation>> GetServices(string[] tags) {
			var result = await _consulClient.Agent.Services();
			return result.Response
				.Select(x => x.Value)
				.Where(x => x.ContainsTags(tags))
				.Select(x => new ServiceLocation {
					Address = x.Address,
					Port = x.Port
				});
		}

		public async Task<ServiceLocation> GetService(string[] tags) {
			var result = await GetServices(tags);
			return result.FirstOrDefault();
		}
	}
}

