using System;
using System.Threading;
using System.Threading.Tasks;
using Cloudburst.ServiceRegistration.Models;
using Consul;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cloudburst.ServiceRegistration {
	public class AgentServiceRegistrationHostedService : IHostedService, IDisposable {
		private readonly ILogger<AgentServiceRegistrationHostedService> _logger;
		private readonly IServiceProvider _serviceProvider;
		private readonly IConsulClient _consulClient;
		private readonly AgentServiceRegistration _serviceRegistration;

		private Task _executingTask;
		private readonly CancellationTokenSource _stoppingCts = new CancellationTokenSource();

		public AgentServiceRegistrationHostedService(ILogger<AgentServiceRegistrationHostedService> logger, IServiceProvider serviceProvider, IConsulClient consulClient, AgentServiceRegistration serviceRegistration) {
			_logger = logger;
			_serviceProvider = serviceProvider;
			_consulClient = consulClient ?? throw new ArgumentNullException(nameof(consulClient));
			_serviceRegistration = serviceRegistration ?? throw new ArgumentNullException(nameof(serviceRegistration));
		}

		public Task StartAsync(CancellationToken cancellationToken) {
			Policies.RetryPolicy(60, _logger)
				.ExecuteAsync(async token => {
						if (!IsValidServiceRegistration()) {
							LogBadServiceRegistration();
							return;
						}
						await _consulClient.Agent.ServiceRegister(_serviceRegistration, token);
					},
					cancellationToken);

			_executingTask = ExecuteAsync(_stoppingCts.Token);

			return _executingTask.IsCompleted ? _executingTask : Task.CompletedTask;
		}

		public async Task StopAsync(CancellationToken cancellationToken) {
			_logger.LogInformation($"AgentServiceRegistrationHostedService is stopping.");

			// We don't need a Polly Policy on the Deregister because if it fails,
			// the service is going to be dead soon and can't retry.
			// Consul Health Checks will take care of it at that point anyway.
#pragma warning disable 4014
			_consulClient.Agent.ServiceDeregister(_serviceRegistration.ID, cancellationToken);
#pragma warning restore 4014

			if (_executingTask is null) {
				return;
			}

			try {
				_stoppingCts.Cancel();
			} finally {
				await Task.WhenAny(_executingTask, Task.Delay(Timeout.Infinite, cancellationToken));
			}
		}

		protected async Task ExecuteAsync(CancellationToken cancellationToken) {
			_logger.LogInformation($"AgentServiceRegistrationHostedService is running.");

			using (var scope = _serviceProvider.CreateScope()) {
				var options = scope.ServiceProvider.GetService<IOptionsMonitor<ServiceDiscoveryOptions>>();

				// If the network between Consul and the microservice is disconnected,
				// the Consul health checks will eventually purge the service from the registry.
				// (Or the Consul service is restarted...)
				// We need to re-register the service once we are connected again.
				while (!cancellationToken.IsCancellationRequested) {
					//Console.WriteLine($"[Agent Service Registration] {_serviceRegistration.Name} {_serviceRegistration.Address}:{_serviceRegistration.Port} {options.CurrentValue.UpdateFrequency}");
					int optionUpdateFrequency = options.CurrentValue.UpdateFrequency;

					if (!IsValidServiceRegistration() || optionUpdateFrequency == -1) {
						await Task.Delay(TimeSpan.FromSeconds(ServiceDiscoveryOptions.DEFAULT_SERVICE_REGISTRATION), cancellationToken);
						continue;
					}

					if (optionUpdateFrequency == 0 || optionUpdateFrequency < ServiceDiscoveryOptions.DEFAULT_SERVICE_REGISTRATION) {
						optionUpdateFrequency = ServiceDiscoveryOptions.DEFAULT_SERVICE_REGISTRATION;
					}

					// We don't need a Retry Policy here because we are going to retry every UPDATE_FREQUENCY anyway.
					await _consulClient.Agent.ServiceRegister(_serviceRegistration, cancellationToken);
					await Task.Delay(TimeSpan.FromSeconds(optionUpdateFrequency), cancellationToken);
				}
			}
		}

		private bool IsValidServiceRegistration() =>
			!(_serviceRegistration.Address is null) &&
			!_serviceRegistration.Address.Contains("InvalidAddress.local") &&
			_serviceRegistration.Port != 0;

		private void LogBadServiceRegistration() {
			_logger.LogWarning("Service Registration could not determine the correct address/port. Service will not be registered in the discovery server.");
		}

		public void Dispose() {
			_consulClient?.Dispose();
			_stoppingCts.Cancel();
		}
	}
}
