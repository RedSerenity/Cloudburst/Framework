﻿using System;
using System.Collections.Generic;
using Cloudburst.Configuration.Extensions.Options;
using Cloudburst.ServiceRegistration;
using Cloudburst.ServiceRegistration.Abstractions;
using Cloudburst.ServiceRegistration.Models;
using Cloudburst.Version.Abstractions;
using Consul;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.ServiceRegistration {
	public static class ServiceCollectionExtensions {

		public static IServiceCollection AddServiceRegistrationHostedService(this IServiceCollection services) {
			return services.AddHostedService<AgentServiceRegistrationHostedService>();
		}

		public static IServiceCollection AddAgentServiceRegistration(this IServiceCollection services) {
			return services.AddAgentServiceRegistration(options => { });
		}

		public static IServiceCollection AddAgentServiceRegistration(this IServiceCollection services, Action<AgentServiceRegistration> options) {
			services.TryAddSingleton(serviceProvider => {
				ServerHelper serverHelper;

				var config = serviceProvider.GetService<ServiceDiscoveryOptions>();
				var environment = serviceProvider.GetService<IWebHostEnvironment>();
				var version = serviceProvider.GetService<IServiceVersion>();

				using (var scope = serviceProvider.CreateScope()) {
					serverHelper = scope.ServiceProvider.GetService<ServerHelper>(); // Must be in scope
				}

				if (environment is null) {
					Console.WriteLine($"ServiceRegistration: {nameof(IWebHostEnvironment)} is null");
					throw new InvalidOperationException();
				}

				if (serverHelper is null) {
					Console.WriteLine($"ServiceRegistration: {nameof(ServerHelper)} is null");
					throw new InvalidOperationException();
				}

				if (config is null) {
					Console.WriteLine($"ServiceRegistration: {nameof(ServiceDiscoveryOptions)} is null");
					throw new InvalidOperationException();
				}

				if (version is null) {
					Console.WriteLine($"ServiceRegistration: {nameof(IServiceVersion)} is null");
					throw new InvalidOperationException();
				}

				var tags = new List<string>();
				if (!(config.ServiceTags is null)) {
					tags.AddRange(config.ServiceTags);
				}

				tags.AddRange(new[] {
					config.ServiceName,
					version.ToString(),
					$"{config.ServiceName} {version}",
					version.UniqueInstanceIdentifier.ToString(),
					environment.EnvironmentName
				});

				string serviceAddress = !String.IsNullOrEmpty(config.ServiceAddress) ?
						config.ServiceAddress :
						serverHelper.GetIpAddress()?.ToString() ?? "InvalidAddress.local";
				int servicePort = config.ServicePort != 0 ?
					config.ServicePort :
					serverHelper.GetPort() != 0 ? serverHelper.GetPort() : 80;

				string healthCheckAddress = String.IsNullOrEmpty(config.Check.OverrideAddress) ?
					$"http://{serviceAddress}:{servicePort}" : config.Check.OverrideAddress;

				string healthCheckPath = String.IsNullOrEmpty(config.Check.OverridePath) ?
					"/health" : $"{config.Check.OverridePath}";

				if (config.Check.AppendGuid) {
					healthCheckPath = $"{healthCheckPath}/{version.UniqueInstanceIdentifier}";
				}

				var healthCheckUri = new UriBuilder(healthCheckAddress) { Path = healthCheckPath };

				var registration = new AgentServiceRegistration {
					// A Unique Identifier for this specific instance
					ID = $"{config.ServiceName} {version} ({version.UniqueInstanceIdentifier})",
					// Group scaled services under this name
					Name = $"{config.ServiceName} {version}",
					Address = serviceAddress,
					Port = servicePort,
					Tags = tags.ToArray(),
					Check = new AgentServiceCheck {
						HTTP = healthCheckUri.ToString(),
						Timeout = TimeSpan.FromSeconds(config.Check.Timeout),
						Interval = TimeSpan.FromSeconds(config.Check.Interval),
						TLSSkipVerify = !config.Check.TlsVerify,
						DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(config.Check.DeregisterCriticalServiceAfter)
					}
				};

				options?.Invoke(registration);

				return registration;
			});

			return services;
		}

		public static IServiceCollection AddCloudServiceRegistration(this IServiceCollection services) {
			return services.AddCloudServiceRegistration(agentOptions => { });
		}

		public static IServiceCollection AddCloudServiceRegistration(this IServiceCollection services, Action<AgentServiceRegistration> agentOptions) {
			services.TryAddScoped<ServerHelper>();
			services.TryAddScoped<IRegisteredServiceLookup, RegisteredServiceLookup>();

			return services.AddAgentServiceRegistration(agentOptions);
		}

		public static IServiceCollection AddCloudServiceDiscoveryOptions(this IServiceCollection services, IConfiguration configuration) {
			services.BindOption<ServiceDiscoveryOptions>(configuration);
			return services;
		}
	}
}
