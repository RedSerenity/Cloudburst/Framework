using System.Collections.Generic;
using System.Linq;
using Consul;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.ServiceRegistration {
	public static class ServiceTagsExtensions {
		public static bool ContainsTags(this AgentService source, string[] subset) {
			if (subset.Length == 0) {
				return true;
			}

			var hashSet = new HashSet<string>(subset.AsEnumerable(), EqualityComparer<string>.Default);
			if (hashSet.Count == 0) {
				return true;
			}

			foreach (var item in source.Tags) {
				hashSet.Remove(item);
				if (hashSet.Count == 0) {
					break;
				}
			}

			return hashSet.Count == 0;
		}
	}
}
