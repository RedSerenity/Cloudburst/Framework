using System;
using Consul;
using Microsoft.Extensions.Hosting;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.ServiceRegistration {
	public static class HostBuilderExtensions {
		public static IHostBuilder UseCloudServiceRegistration(this IHostBuilder builder) {
			return builder.UseCloudServiceRegistration(agentOptions => { });
		}

		public static IHostBuilder UseCloudServiceRegistration(this IHostBuilder builder, Action<AgentServiceRegistration> agentOptions) {
			return builder.ConfigureServices((buildContext, services) => {
				services.AddCloudServiceDiscoveryOptions(buildContext.Configuration);
				services.AddCloudServiceRegistration(agentOptions);
				services.AddServiceRegistrationHostedService();
			});
		}
	}
}
