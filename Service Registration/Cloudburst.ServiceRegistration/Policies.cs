using System;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Retry;

namespace Cloudburst.ServiceRegistration {
	public class Policies {
		public static AsyncRetryPolicy RetryPolicy(int maxWait = 60, ILogger logger = null) =>
			Policy
				.Handle<HttpRequestException>()
				.Or<OperationCanceledException>()
				.WaitAndRetryForeverAsync(
					retryAttempt => {
						var waitTime = TimeSpan.FromSeconds(Math.Pow(2, retryAttempt));
						return waitTime.Seconds > maxWait ? TimeSpan.FromSeconds(maxWait) : waitTime;
					},
					(exception, timeSpan, context) => { logger?.LogError(exception.Message, exception); }
				);
	}
}
