using Cloudburst.Cache;
using Cloudburst.Cache.Abstractions;
using Cloudburst.Cache.Consul.Extensions;
using Cloudburst.Cache.Models;
using Cloudburst.Configuration.Extensions.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Cache {
	public static class ServiceCollectionExtensions {
		public static IServiceCollection AddCloudCache(this IServiceCollection services) {
			services.TryAddScoped<ICloudCache, CloudCache>();
			return services;
		}

		public static IServiceCollection AddCloudCacheWithInMemoryProvider(this IServiceCollection services) {
			return services
				.AddDistributedMemoryCache()
				.AddCloudCache();

		}

		public static IServiceCollection AddCloudCacheWithConsulProvider(this IServiceCollection services) {
			return services
				.AddDistributedConsulCache()
				.AddCloudCache();
		}

		public static IServiceCollection AddCloudCacheWithConsulProvider(this IServiceCollection services, IConfiguration configuration) {
			services.BindOption<ConsulCacheOptions>(configuration);

			return services
				.AddDistributedConsulCache()
				.AddCloudCache();
		}
	}
}
