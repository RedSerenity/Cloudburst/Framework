﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using Cloudburst.Cache.Abstractions;
using Cloudburst.Cache.Models;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;

namespace Cloudburst.Cache {
	public class CloudCache : ICloudCache {
		private readonly IDistributedCache _cache;

		private readonly DistributedCacheEntryOptions _cacheOptions;

		public CloudCache(IDistributedCache cache, IOptionsMonitor<ConsulCacheOptions> options) {
			_cache = cache;

			_cacheOptions = new DistributedCacheEntryOptions();

			if (options.CurrentValue.SlidingExpirationSeconds > 0) {
				_cacheOptions.SetSlidingExpiration(TimeSpan.FromSeconds(options.CurrentValue.SlidingExpirationSeconds));
			}

			if (options.CurrentValue.AbsoluteExpirationSeconds > 0) {
				_cacheOptions.SetAbsoluteExpiration(TimeSpan.FromSeconds(options.CurrentValue.AbsoluteExpirationSeconds));
			}
		}

		public T GetOrFetch<T>(string cacheKey, Func<T> fetchFunc) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			var cachedData = _cache.Get(cacheKey);

			if (!(cachedData is null)) {
				return GetObject<T>(cachedData);
			}

			T result = fetchFunc.Invoke();

			if (result is null) {
				return default;
			}

			Set(cacheKey, result);

			return result;
		}

		public async Task<T> GetOrFetchAsync<T>(string cacheKey, Func<CancellationToken, Task<T>> fetchFunc, CancellationToken cancellationToken = default) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			var cachedData = await _cache.GetAsync(cacheKey, cancellationToken);

			if (!(cachedData is null)) {
				return GetObject<T>(cachedData);
			}

			T result = await fetchFunc.Invoke(cancellationToken);

			if (result is null) {
				return default;
			}

			await SetAsync(cacheKey, result, cancellationToken);

			return result;
		}

		public T Get<T>(string cacheKey) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			var cachedData = _cache.Get(cacheKey);
			return cachedData is null ? default : GetObject<T>(cachedData);
		}

		public async Task<T> GetAsync<T>(string cacheKey, CancellationToken cancellationToken = default) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			var cachedData = await _cache.GetAsync(cacheKey, cancellationToken);
			return cachedData is null ? default : GetObject<T>(cachedData);
		}

		public void Set<T>(string cacheKey, T entity) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			switch (entity) {
				case null:
					_cache.Remove(cacheKey);
					break;
				case string entityString:
					_cache.SetString(cacheKey, entityString, _cacheOptions);
					break;
				default:
					var dataToCache = GetBytes(entity);
					_cache.Set(cacheKey, dataToCache, _cacheOptions);
					break;
			}
		}

		public async Task SetAsync<T>(string cacheKey, T entity, CancellationToken cancellationToken = default) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			switch (entity) {
				case null:
					await _cache.RemoveAsync(cacheKey, cancellationToken);
					break;
				case string entityString:
					await _cache.SetStringAsync(cacheKey, entityString, _cacheOptions, cancellationToken);
					break;
				default:
					var dataToCache = GetBytes(entity);
					await _cache.SetAsync(cacheKey, dataToCache, _cacheOptions, cancellationToken);
					break;
			}
		}

		public void Refresh(string cacheKey) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			_cache.Refresh(cacheKey);
		}

		public async Task RefreshAsync(string cacheKey, CancellationToken cancellationToken = default) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			await _cache.RefreshAsync(cacheKey, cancellationToken);
		}

		public void Remove(string cacheKey) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			_cache.Remove(cacheKey);
		}

		public async Task RemoveAsync(string cacheKey, CancellationToken cancellationToken = default) {
			if (String.IsNullOrEmpty(cacheKey)) {
				throw new ArgumentNullException(nameof(cacheKey));
			}

			await _cache.RemoveAsync(cacheKey, cancellationToken);
		}

		private byte[] GetBytes<T>(T obj) {
			if (obj is null) {
				return null;
			}

			BinaryFormatter binaryFormatter = new BinaryFormatter();
			using (MemoryStream memoryStream = new MemoryStream()) {
				binaryFormatter.Serialize(memoryStream, obj);
				return memoryStream.ToArray();
			}
		}

		private T GetObject<T>(byte[] data) {
			if (data is null) {
				return default;
			}

			BinaryFormatter binaryFormatter = new BinaryFormatter();
			using (MemoryStream memoryStream = new MemoryStream(data)) {
				object obj = binaryFormatter.Deserialize(memoryStream);
				return (T) obj;
			}
		}
	}
}
