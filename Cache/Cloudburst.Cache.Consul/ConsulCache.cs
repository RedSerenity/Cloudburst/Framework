﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Cloudburst.Cache.Models;
using Consul;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

#pragma warning disable 4014

namespace Cloudburst.Cache.Consul {
	public class ConsulCache : IDistributedCache {
		private const int CONSUL_STORAGE_LIMIT = 512000;

		private readonly ILogger<ConsulCache> _logger;
		private readonly IConsulClient _client;
		private readonly IOptionsMonitor<ConsulCacheOptions> _options;

		public ConsulCache(ILogger<ConsulCache> logger, IConsulClient client, IOptionsMonitor<ConsulCacheOptions> options) {
			_logger = logger;
			_client = client;
			_options = options;
		}

		public byte[] Get(string key) {
			if (String.IsNullOrEmpty(key)) {
				throw new ArgumentNullException(nameof(key));
			}

			string realKey = BuildKey(key);
			var result = Task.Run(() => _client.KV.Get(realKey)).Result;

			if (result.Response is null) {
				return null;
			}

			if (HasExpired(result.Response.Flags)) {
				Task.Run(() => RemoveAsync(realKey));
				return null;
			}

			Task.Run(() => RefreshSessionAsync(result));

			return result.Response.Value;
		}

		public async Task<byte[]> GetAsync(string key, CancellationToken token = new CancellationToken()) {
			if (String.IsNullOrEmpty(key)) {
				throw new ArgumentNullException(nameof(key));
			}

			string realKey = BuildKey(key);
			var result = await _client.KV.Get(realKey, token);

			if (result.Response is null) {
				return null;
			}

			if (HasExpired(result.Response.Flags)) {
				RemoveAsync(realKey, token);
				return null;
			}

			RefreshSessionAsync(result, token);

			return result.Response.Value;
		}

		public void Set(string key, byte[] value, DistributedCacheEntryOptions options) {
			if (String.IsNullOrEmpty(key)) {
				throw new ArgumentNullException(nameof(key));
			}

			if (value.Length > CONSUL_STORAGE_LIMIT) {
				_logger.LogError($"Consul Cache KeyValue storage size limit of {CONSUL_STORAGE_LIMIT} exceeded by {value.Length - CONSUL_STORAGE_LIMIT} bytes");
				throw new CacheSizeLimitExceededException(value.Length);
			}

			string realKey = BuildKey(key);

			WriteResult<string> session = null;
			if (!(options.SlidingExpiration is null)) {
				var ttl = options.SlidingExpiration.Value.TotalSeconds < 10 ?
					10 : options.SlidingExpiration.Value.TotalSeconds > 86400 ?
						86400 : options.SlidingExpiration.Value.TotalSeconds;

				var sessionEntry = new SessionEntry {
					TTL = TimeSpan.FromSeconds(ttl),
					Behavior = SessionBehavior.Delete,
					Name = realKey
				};

				session = Task.Run(() => _client.Session.Create(sessionEntry)).Result;
			}

			ulong expiresAt = 0;
			if (options.AbsoluteExpirationRelativeToNow.HasValue) {
				expiresAt = (ulong) (
					SecondsSince2000() + options.AbsoluteExpirationRelativeToNow.Value.TotalSeconds
				);
			}

			var kvPair = new KVPair(realKey) {
				Value = value,
				Session = session?.Response,
				Flags = expiresAt
			};

			var result = Task.Run(() => _client.KV.Put(kvPair)).Result;
			if (!result.Response) {
				_logger.LogError($"Could not put {realKey} into Consul KV Store.");
			}
		}

		public async Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = new CancellationToken()) {
			if (String.IsNullOrEmpty(key)) {
				throw new ArgumentNullException(nameof(key));
			}

			if (value.Length > CONSUL_STORAGE_LIMIT) {
				_logger.LogError($"Consul Cache KeyValue storage size limit of {CONSUL_STORAGE_LIMIT} exceeded by {value.Length - CONSUL_STORAGE_LIMIT} bytes");
				throw new CacheSizeLimitExceededException(value.Length);
			}

			string realKey = BuildKey(key);

			WriteResult<string> session = null;
			if (!(options.SlidingExpiration is null)) {
				var ttl = options.SlidingExpiration.Value.TotalSeconds < 10 ?
					10 : options.SlidingExpiration.Value.TotalSeconds > 86400 ?
					86400 : options.SlidingExpiration.Value.TotalSeconds;

				var sessionEntry = new SessionEntry {
					TTL = TimeSpan.FromSeconds(ttl),
					Behavior = SessionBehavior.Delete,
					Name = realKey
				};

				session = await _client.Session.Create(sessionEntry, token);
			}

			ulong expiresAt = 0;
			if (options.AbsoluteExpirationRelativeToNow.HasValue) {
				expiresAt = (ulong) (
					SecondsSince2000() + options.AbsoluteExpirationRelativeToNow.Value.TotalSeconds
				);
			}

			var kvPair = new KVPair(realKey) {
				Value = value,
				Session = session?.Response,
				Flags = expiresAt
			};

			var result = await _client.KV.Put(kvPair, token);
			if (!result.Response) {
				_logger.LogError($"Could not put {realKey} into Consul KV Store.");
			}
		}

		public void Refresh(string key) {
			if (String.IsNullOrEmpty(key)) {
				throw new ArgumentNullException(nameof(key));
			}

			string realKey = BuildKey(key);

			Get(realKey);
		}

		public async Task RefreshAsync(string key, CancellationToken token = new CancellationToken()) {
			if (String.IsNullOrEmpty(key)) {
				throw new ArgumentNullException(nameof(key));
			}

			string realKey = BuildKey(key);

			await GetAsync(realKey, token);
		}

		public void Remove(string key) {
			if (String.IsNullOrEmpty(key)) {
				throw new ArgumentNullException(nameof(key));
			}

			string realKey = BuildKey(key);

			Task.Run(() => _client.KV.Delete(realKey));
		}

		public async Task RemoveAsync(string key, CancellationToken token = new CancellationToken()) {
			if (String.IsNullOrEmpty(key)) {
				throw new ArgumentNullException(nameof(key));
			}

			string realKey = BuildKey(key);

			await _client.KV.Delete(realKey, token);
		}

		private async Task RefreshSessionAsync(QueryResult<KVPair> result, CancellationToken token = default) {
			var sessionId = result.Response.Session;

			if (String.IsNullOrEmpty(sessionId)) {
				return;
			}

			await _client.Session.Renew(sessionId, token);
		}

		private static long SecondsSince2000() => (long) (DateTime.UtcNow - new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
		private static bool HasExpired(ulong expiresAt) => expiresAt != 0 && (SecondsSince2000() - (long) expiresAt) > 0;

		private string BuildKey(string key) {
			string rootKey = _options.CurrentValue.RootKey?.TrimEnd('/') ?? String.Empty;
			string processedKey = key.Replace(':', '/');
			return $"{rootKey}/{processedKey}".TrimStart('/');
		}
	}
}
