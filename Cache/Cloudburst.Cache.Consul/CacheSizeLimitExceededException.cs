using System;
using System.Runtime.Serialization;

namespace Cloudburst.Cache.Consul {
	public class CacheSizeLimitExceededException : Exception {
		private const int CONSUL_STORAGE_LIMIT = 512000;

		protected CacheSizeLimitExceededException(SerializationInfo info, StreamingContext context) : base(info, context) { }
		public CacheSizeLimitExceededException(int size) : base($"Consul Cache KeyValue storage size limit of {CONSUL_STORAGE_LIMIT} exceeded by {size - CONSUL_STORAGE_LIMIT} bytes") { }
		public CacheSizeLimitExceededException(int size, Exception innerException) : base($"Consul Cache KeyValue storage size limit of {CONSUL_STORAGE_LIMIT} exceeded by {size - CONSUL_STORAGE_LIMIT} bytes", innerException) { }
	}
}
