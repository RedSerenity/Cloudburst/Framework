using System;
using Cloudburst.Cache.Models;
using Cloudburst.Configuration.Extensions.Options;
using Cloudburst.Configuration.Models;
using Consul;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Cloudburst.Cache.Consul.Extensions {
	public static class ServiceCollectionExtensions {
		public static IServiceCollection AddDistributedConsulCache(this IServiceCollection services) {
			services.AddSingleton<IDistributedCache, ConsulCache>();
			return services;
		}
	}
}
