﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Cloudburst.Cache.Abstractions {
	public interface ICloudCache {
		T GetOrFetch<T>(string cacheKey, Func<T> fetchFunc);
		Task<T> GetOrFetchAsync<T>(string cacheKey, Func<CancellationToken, Task<T>> fetchFunc, CancellationToken cancellationToken = default);
		T Get<T>(string cacheKey);
		Task<T> GetAsync<T>(string cacheKey, CancellationToken cancellationToken = default);
		void Set<T>(string cacheKey, T entity);
		Task SetAsync<T>(string cacheKey, T entity, CancellationToken cancellationToken = default);
		void Refresh(string cacheKey);
		Task RefreshAsync(string cacheKey, CancellationToken cancellationToken = default);
		void Remove(string cacheKey);
		Task RemoveAsync(string cacheKey, CancellationToken cancellationToken = default);
	}
}
