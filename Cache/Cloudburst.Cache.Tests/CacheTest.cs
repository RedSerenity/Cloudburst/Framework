using System;
using System.Threading.Tasks;
using Cloudburst.Cache.Consul;
using Cloudburst.Cache.Models;
using Consul;
using Divergic.Logging.Xunit;
using Microsoft.Extensions.Options;
using Xunit;
using Xunit.Abstractions;

namespace Cloudburst.Cache.Tests {
	internal class CacheOptionsMonitor : IOptionsMonitor<ConsulCacheOptions> {
		public CacheOptionsMonitor(ConsulCacheOptions currentValue) {
			CurrentValue = currentValue;
		}

		public ConsulCacheOptions Get(string name) {
			return CurrentValue;
		}

		public IDisposable OnChange(Action<ConsulCacheOptions, string> listener) {
			throw new NotImplementedException();
		}

		public ConsulCacheOptions CurrentValue { get; }
	}

	public class CacheTest {
		private const string CACHE_KEY = "Test/Key";

		private TestData _testData { get;  } =
			new TestData {
				Hello = "World",
				Goodbye = "Universe"
			};

		private readonly ITestOutputHelper _output;
		private readonly ICacheLogger<ConsulCache> _logger;
		private readonly ConsulClient _consulClient;
		private readonly ConsulCache _consulCache;
		private readonly CacheOptionsMonitor _options;
		private readonly CloudCache _cloudCache;

		public CacheTest(ITestOutputHelper output) {
			_output = output;

			_options = new CacheOptionsMonitor(
				new ConsulCacheOptions {
					AbsoluteExpirationSeconds = 45,
					SlidingExpirationSeconds = 10,
					RootKey = "Test/Root"
				}
			);

			_logger = output.BuildLoggerFor<ConsulCache>();
			_consulClient = new ConsulClient(config => {
				config.Address = new Uri("http://localhost:8500");
				config.Datacenter = "dc1";
			});
			_consulCache = new ConsulCache(_logger, _consulClient, _options);
			_cloudCache = new CloudCache(_consulCache, _options);
		}

		[Fact]
		public async Task SetAndGetKey() {
			await _cloudCache.SetAsync($"{CACHE_KEY}SetGet", _testData);
			var result = await _cloudCache.GetAsync<TestData>($"{CACHE_KEY}SetGet");

			Assert.NotNull(result);
			Assert.Equal(_testData.Hello, result.Hello);
			Assert.Equal(_testData.Goodbye, result.Goodbye);

			await _cloudCache.RemoveAsync($"{CACHE_KEY}SetGet");
		}

		[Fact]
		public async Task DeleteKey() {
			var existingResult = await _cloudCache.GetAsync<TestData>($"{CACHE_KEY}Delete");

			if (existingResult is null) {
				await _cloudCache.SetAsync($"{CACHE_KEY}Delete", _testData);
			}

			await _cloudCache.RemoveAsync($"{CACHE_KEY}Delete");

			var result = await _cloudCache.GetAsync<TestData>($"{CACHE_KEY}Delete");

			Assert.Null(result);
		}

		[Fact]
		public async Task SlidingExpiration() {
			await _cloudCache.RemoveAsync($"{CACHE_KEY}Slide");
			await _cloudCache.SetAsync($"{CACHE_KEY}Slide", _testData);

			await Task.Delay(25 * 1000);
			var result = await _cloudCache.GetAsync<TestData>($"{CACHE_KEY}Slide");

			Assert.Null(result);

			await _cloudCache.RemoveAsync($"{CACHE_KEY}Slide");
		}

		[Fact]
		public async Task AbsoluteExpiration() {
			await _cloudCache.RemoveAsync($"{CACHE_KEY}Absolute");
			await _cloudCache.SetAsync($"{CACHE_KEY}Absolute", _testData);

			await Task.Delay(9 * 1000); // 9 Seconds
			var resultOne = await _cloudCache.GetAsync<TestData>($"{CACHE_KEY}Absolute");
			Assert.NotNull(resultOne);
			Assert.Equal(_testData.Hello, resultOne.Hello);

			await Task.Delay(9 * 1000); // 18 Seconds
			var resultTwo = await _cloudCache.GetAsync<TestData>($"{CACHE_KEY}Absolute");
			Assert.NotNull(resultTwo);
			Assert.Equal(_testData.Hello, resultTwo.Hello);

			await Task.Delay(9 * 1000); // 27 Seconds.
			var resultThree = await _cloudCache.GetAsync<TestData>($"{CACHE_KEY}Absolute");
			Assert.NotNull(resultThree);
			Assert.Equal(_testData.Hello, resultThree.Hello);

			await Task.Delay(9 * 1000); // 36 Seconds
			var resultFive = await _cloudCache.GetAsync<TestData>($"{CACHE_KEY}Absolute");
			Assert.NotNull(resultFive);
			Assert.Equal(_testData.Hello, resultFive.Hello);

			await Task.Delay(10 * 1000); // 46 Seconds - Should be dead now.
			var resultSix = await _cloudCache.GetAsync<TestData>($"{CACHE_KEY}Absolute");
			Assert.Null(resultSix);

			await _cloudCache.RemoveAsync($"{CACHE_KEY}Absolute");
		}
	}

	[Serializable]
	public class TestData {
		public string Hello { get; set; }
		public string Goodbye { get; set; }
	}
}
