﻿using Cloudburst.Configuration.Attributes;

namespace Cloudburst.Cache.Models {
	[ConfigurationKey("Cloudburst:Cache")]
	public class ConsulCacheOptions {
		public int SlidingExpirationSeconds { get; set; } = 5;
		public int AbsoluteExpirationSeconds { get; set; } = 30;
		public string RootKey { get; set; } = "Cache";
	}
}
