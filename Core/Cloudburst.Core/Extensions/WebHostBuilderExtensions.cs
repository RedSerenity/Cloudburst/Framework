using Microsoft.AspNetCore.Server.Kestrel.Core;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Core {
	public static class WebHostBuilderExtensions {
		public static KestrelServerOptions ConfigureNonTlsPorts(this KestrelServerOptions options, int httpPort, int http2Port) {
			options.ListenAnyIP(httpPort, listenOptions => {
				listenOptions.Protocols = HttpProtocols.Http1;
			});

			options.ListenAnyIP(http2Port, listenOptions => {
				listenOptions.Protocols = HttpProtocols.Http2;
			});

			return options;
		}
	}
}
