using System;
using Cloudburst.Extensions.Configuration;
using Cloudburst.Extensions.Consul;
using Cloudburst.Extensions.HealthChecks;
using Cloudburst.Extensions.Logging;
using Cloudburst.Extensions.ServiceRegistration;
using Cloudburst.Extensions.Version;
using Cloudburst.Version.Abstractions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace Cloudburst.Core {
	public static class Cloud {
		/* Examples:
			public class Program {
				public static void Main(string[] args) => CreateHostBuilder(args);
				public static void CreateHostBuilder(string[] args) => Cloud.Downpour<Startup, ServiceVersion>(args);
			}

			public class Program {
				public static void Main(string[] args) => CreateHostBuilder(args);
				public static void CreateHostBuilder(string[] args) =>
					Cloud.Downpour<Startup, ServiceVersion>(args, _webHostBuilder);

				private static readonly Action<IWebHostBuilder> _webHostBuilder = webBuilder => {
					webBuilder.ConfigureKestrel(options => {
						options.ListenAnyIP(5000, listenOptions => { listenOptions.Protocols = HttpProtocols.Http1; });
						options.ListenAnyIP(5001, listenOptions => { listenOptions.Protocols = HttpProtocols.Http2; });
					});
				};
			}

			public class Program {
				public static void Main(string[] args) => CreateHostBuilder(args);
				public static void CreateHostBuilder(string[] args) =>
					Cloud.Downpour<Startup, ServiceVersion>(args, _webHostBuilder);

				private static readonly Action<IWebHostBuilder> _webHostBuilder = webBuilder => {
					webBuilder.ConfigureKestrel(options => {
						webBuilder.ConfigureNonTlsPorts(5554, 5555);
					});
				};
			}
		 */

		public static void Downpour<TStartup, TServiceVersion>(string[] args) where TStartup : class where TServiceVersion : class, IServiceVersion {
			Downpour<TStartup, TServiceVersion>(args, webBuilder => { }, hostBuilder => { }, healthChecksBuilder => { });
		}


		public static void Downpour<TStartup, TServiceVersion>(string[] args, Action<IWebHostBuilder> webBuilder) where TStartup : class where TServiceVersion : class, IServiceVersion {
			Downpour<TStartup, TServiceVersion>(args, webBuilder, hostBuilder => { }, healthChecksBuilder => { });
		}

		public static void Downpour<TStartup, TServiceVersion>(string[] args, Action<IWebHostBuilder> webBuilder, Action<IHostBuilder> hostBuilder) where TStartup : class where TServiceVersion : class, IServiceVersion {
			Downpour<TStartup, TServiceVersion>(args, webBuilder, hostBuilder, healthChecksBuilder => { });
		}

		public static void Downpour<TStartup, TServiceVersion>(string[] args, Action<IWebHostBuilder> webBuilder, Action<IHealthChecksBuilder> healthChecksBuilder) where TStartup : class where TServiceVersion : class, IServiceVersion {
			Downpour<TStartup, TServiceVersion>(args, webBuilder, hostBuilder => { }, healthChecksBuilder);
		}


		public static void Downpour<TStartup, TServiceVersion>(string[] args, Action<IHostBuilder> hostBuilder) where TStartup : class where TServiceVersion : class, IServiceVersion {
			Downpour<TStartup, TServiceVersion>(args, webBuilder => { }, hostBuilder, healthChecksBuilder => { });
		}

		public static void Downpour<TStartup, TServiceVersion>(string[] args, Action<IHostBuilder> hostBuilder, Action<IHealthChecksBuilder> healthChecksBuilder) where TStartup : class where TServiceVersion : class, IServiceVersion {
			Downpour<TStartup, TServiceVersion>(args, webBuilder => { }, hostBuilder, healthChecksBuilder);
		}



		public static void Downpour<TStartup, TServiceVersion>(string[] args, Action<IHealthChecksBuilder> healthChecksBuilder) where TStartup : class where TServiceVersion : class, IServiceVersion {
			Downpour<TStartup, TServiceVersion>(args, webBuilder => { }, hostBuilder => { }, healthChecksBuilder);
		}


		public static void Downpour<TStartup, TServiceVersion>(string[] args, Action<IWebHostBuilder> webBuilder, Action<IHostBuilder> hostBuilder, Action<IHealthChecksBuilder> healthChecksBuilder) where TStartup : class where TServiceVersion : class, IServiceVersion {
			try {
				// Create a logger now to log any exceptions during Host Startup.
				// It will be replaced later on with a logger configured from the settings.
				Log.Logger = new LoggerConfiguration()
					.Enrich.FromLogContext()
					.MinimumLevel.Debug()
					.MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
					.MinimumLevel.Override("System", LogEventLevel.Debug)
					.WriteTo.Console(theme: AnsiConsoleTheme.Code)
					.CreateLogger();

				IHostBuilder cloudburstHostBuilder = Host.CreateDefaultBuilder(args)
					.UseCloudConsul()
					.UseCloudVersioning<TServiceVersion>()
					.UseCloudConfiguration<TServiceVersion>()
					.UseCloudLogging()
					.UseCloudServiceRegistration()
					.UseCloudHealthCheck(healthChecksBuilder);

				hostBuilder?.Invoke(cloudburstHostBuilder);

				IHost host = cloudburstHostBuilder
					.ConfigureWebHostDefaults(wb => {
						wb.UseStartup<TStartup>();
						webBuilder?.Invoke(wb);
					})
					.Build();

				host.Run();
			} catch (Exception exception) {
				Console.WriteLine($"[Exception] {exception.Message}");
				Console.WriteLine(exception.StackTrace);
				Log.Fatal(exception, "Host TERMINATED unexpectedly.");
			} finally {
				Log.CloseAndFlush();
			}
		}
	}
}

