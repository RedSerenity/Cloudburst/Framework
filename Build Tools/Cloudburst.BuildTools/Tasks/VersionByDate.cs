using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Cloudburst.BuildTools.Tasks {
	public class VersionByDate : Task {
		[Output]
		public string GeneratedVersion { get; set; }

		public override bool Execute() {
			GeneratedVersion = $"{DateTime.UtcNow.Year}.{DateTime.UtcNow.Month}.{DateTime.UtcNow.Day}";
			return true;
		}
	}
}
