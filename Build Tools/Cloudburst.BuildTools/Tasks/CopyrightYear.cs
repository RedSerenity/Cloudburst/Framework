using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Cloudburst.BuildTools.Tasks {
	public class CopyrightYear : Task {
		[Output]
		public string CurrentYear { get; set; }

		public override bool Execute() {
			CurrentYear = DateTime.UtcNow.ToString("yyyy");
			return true;
		}
	}
}
