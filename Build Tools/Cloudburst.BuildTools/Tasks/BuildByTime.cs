using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Cloudburst.BuildTools.Tasks {
	public class BuildByTime : Task {
		[Output]
		public string GeneratedBuild { get; set; }

		public override bool Execute() {
			GeneratedBuild = $"{Convert.ToUInt16(DateTime.UtcNow.TimeOfDay.TotalSeconds / 1.32)}";
			return true;
		}
	}
}
