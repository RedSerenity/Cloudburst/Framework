using System;
using System.IO;
using Microsoft.Build.Utilities;

namespace Cloudburst.BuildTools.Tasks {
	public enum ConfigurationType {
		Debug,
		Release
	}

	public class OutputVersion : Task {
		private const string VERSION_FILE = "Version.props";

		private string VersionFile => !String.IsNullOrEmpty(VersionFilePath) ? Path.GetFullPath($"{VersionFilePath}/{VERSION_FILE}") : VERSION_FILE;

		private ConfigurationType _configuration;

		public string Configuration {
			get => _configuration.ToString();
			set {
				try {
					_configuration = (ConfigurationType) Enum.Parse(typeof(ConfigurationType), value);
				} catch (Exception) {
					_configuration = ConfigurationType.Debug;
				}
			}
		}

		public string VersionFilePath { get; set; }

		public override bool Execute() {
			var generatedVersion = $"{DateTime.UtcNow.Year}.{DateTime.UtcNow.Month}.{DateTime.UtcNow.Day}";
			var generatedBuild = $"{Convert.ToUInt16(DateTime.UtcNow.TimeOfDay.TotalSeconds / 1.32)}";

			string version;

			if (_configuration == ConfigurationType.Debug) {
				version = $"{generatedVersion}.{generatedBuild}-beta";
			} else {
				version = $"{generatedVersion}.0";
			}

			string versionContents = $"<Project>\n\t<PropertyGroup>\n\t\t<Version>{version}</Version>\n\t\t<PackageVersion>{version}</PackageVersion>\n\t</PropertyGroup>\n</Project>";

			File.WriteAllText(VersionFile, versionContents);

			return true;
		}
	}
}
