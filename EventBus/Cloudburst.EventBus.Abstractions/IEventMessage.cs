﻿using System;

namespace Cloudburst.EventBus.Abstractions {
	public interface IEventMessage {
		Guid EventId { get; }
		DateTime Timestamp { get; }
	}
}
