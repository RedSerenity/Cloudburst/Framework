﻿using System;
using Cloudburst.EventBus.Models;
using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using MassTransit.RabbitMqTransport;

namespace Cloudburst.EventBus.RabbitMq.Extensions {
	public static class ServiceCollectionExtensions {
		public static IServiceCollectionBusConfigurator AddRabbitMq(this IServiceCollectionBusConfigurator busConfigurator) {
			return busConfigurator.AddRabbitMq(new EventBusOptions { ConnectionString = "rabbitmq://localhost" }, (context, config) => { });
		}

		public static IServiceCollectionBusConfigurator AddRabbitMq(this IServiceCollectionBusConfigurator busConfigurator, EventBusOptions options) {
			return busConfigurator.AddRabbitMq(options, (context, config) => { });
		}

		public static IServiceCollectionBusConfigurator AddRabbitMq(this IServiceCollectionBusConfigurator busConfigurator, EventBusOptions options, Action<IBusRegistrationContext, IRabbitMqBusFactoryConfigurator> configurator) {
			busConfigurator.UsingRabbitMq((context, config) => {
				config.ConfigureEndpoints(context);
				config.UseHealthCheck(context);
				config.Host(options.ConnectionString);

				configurator.Invoke(context, config);
			});

			return busConfigurator;
		}
	}
}
