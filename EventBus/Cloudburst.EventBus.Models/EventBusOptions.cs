﻿using System;
using Cloudburst.Configuration.Attributes;

namespace Cloudburst.EventBus.Models {
	[ConfigurationKey("Cloudburst:EventBus")]
	public class EventBusOptions {
		public string ConnectionString { get; set; }
	}
}
