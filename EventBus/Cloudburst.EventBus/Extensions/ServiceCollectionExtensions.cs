﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cloudburst.Configuration.Extensions.Options;
using Cloudburst.EventBus.Models;
using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cloudburst.EventBus.Extensions {
	public static class ServiceCollectionExtensions {
		private static readonly IList<string> _excludedAssemblies = new List<string> {
			"System", "Microsoft", "netstandard", "Serilog",
			"Consul", "Polly", "Newtonsoft", "MassTransit"
		};

		public static IServiceCollection AddCloudEventBus(this IServiceCollection services, Action<IServiceCollectionBusConfigurator> configurator) {
			if (configurator is null) {
				throw new ArgumentNullException(nameof(configurator));
			}

			var assemblies = AppDomain.CurrentDomain.GetAssemblies()
				.Where(x => !_excludedAssemblies.Any(y => x.FullName.StartsWith(y)))
				.ToArray();

			services.AddMassTransit(config => {
				config.AddConsumers(assemblies);
				config.AddSagas(assemblies);
				config.AddActivities(assemblies);
				config.AddSagaStateMachines(assemblies);

				configurator.Invoke(config);
			});

			services.AddMassTransitHostedService();
			return services;
		}

		public static IServiceCollection AddCloudEventBusOptions(this IServiceCollection services, IConfiguration configuration) {
			services.BindOption<EventBusOptions>(configuration);
			return services;
		}
	}
}
