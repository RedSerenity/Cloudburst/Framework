using System;
using Cloudburst.Configuration.Extensions.Options;
using Cloudburst.EventBus.Models;
using Cloudburst.EventBus.RabbitMq.Extensions;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using Microsoft.Extensions.Hosting;

namespace Cloudburst.EventBus.Extensions {
	public static class HostBuilderExtensions {
		public static IHostBuilder UseCloudEventBus(this IHostBuilder builder, Action<IServiceCollectionBusConfigurator> busConfigurator) {
			builder.ConfigureServices((context, services) => {
				services
					.AddCloudEventBusOptions(context.Configuration)
					.AddCloudEventBus(busConfigurator.Invoke);
			});

			return builder;
		}

		public static IHostBuilder UseCloudEventBusWithRabbitMq(this IHostBuilder builder) {
			builder.ConfigureServices((context, services) => {
				var eventBusOptions = context.Configuration.GetWithKey<EventBusOptions>();

				services
					.AddCloudEventBusOptions(context.Configuration)
					.AddCloudEventBus(busConfigurator => busConfigurator.AddRabbitMq(eventBusOptions));
			});

			return builder;
		}
	}
}
