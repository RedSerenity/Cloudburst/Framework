﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Logging {
	public static class HostBuilderExtensions {
		public static IHostBuilder UseCloudLogging(this IHostBuilder builder) {
			builder.ConfigureAppConfiguration((builderContext, config) => {
				IConfigurationRoot currentConfig = config.Build();
				Log.Logger = BuildDefaultLogger(currentConfig, builderContext.HostingEnvironment.IsDevelopment());
			});

			builder.ConfigureLogging((builderContext, logging) => {
				logging.AddSerilog();
			});

			builder.UseSerilog();

			return builder;
		}

		private static Logger BuildDefaultLogger(IConfigurationRoot config, bool isDevelopment) {
			var hasConfig = config.GetSection("Serilog").Exists();

			var loggerConfig = new LoggerConfiguration()
				.Enrich.FromLogContext();

			if (hasConfig) {
				return loggerConfig
					.ReadFrom.Configuration(config)
					.CreateLogger();
			}

			if (isDevelopment) {
				loggerConfig.MinimumLevel.Debug()
					.MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
					.MinimumLevel.Override("System", LogEventLevel.Debug);
			}

			return loggerConfig.CreateLogger();
		}
	}
}
