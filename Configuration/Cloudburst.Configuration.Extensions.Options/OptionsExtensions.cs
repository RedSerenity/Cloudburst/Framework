﻿using System;
using System.Reflection;
using Cloudburst.Configuration.Attributes;
using Cloudburst.Configuration.Exceptions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

namespace Cloudburst.Configuration.Extensions.Options {
	public static class OptionsExtensions {
		public static T BindOption<T>(this IServiceCollection services, IConfiguration configuration) where T : class {
			string configurationKey = GetConfigurationKey<T>();
			return services.BindOption<T>(configuration, configurationKey);
		}

		public static T BindOption<T>(this IServiceCollection services, IConfiguration configuration, string configurationKey) where T : class {
			var configSection = configuration.GetSection(configurationKey);

			if (String.IsNullOrEmpty(configurationKey)) {
				throw new ArgumentNullException(nameof(configurationKey));
			}

			if (configSection is null || !configSection.Exists()) {
				throw new MissingConfigurationSectionException(configurationKey, typeof(T));
			}

			services.AddOptions<T>()
				.Bind(configSection)
				.ValidateDataAnnotations();

			T result = configSection.Get<T>();
			services.TryAddSingleton(result);
			return result;
		}

		public static T GetWithKey<T>(this IConfiguration configuration) {
			string configurationKey = GetConfigurationKey<T>();
			var option = configuration.GetSection(configurationKey).Get<T>();

			if (option is null) {
				throw new MissingConfigurationSectionException(configurationKey, typeof(T));
			}

			return option;
		}

		public static string GetConfigurationKey<T>() {
			ConfigurationKeyAttribute configurationKey = typeof(T).GetCustomAttribute<ConfigurationKeyAttribute>();

			if (configurationKey is null) {
				throw new ConfigurationKeyAttributeMissingException(typeof(T));
			}

			if (String.IsNullOrEmpty(configurationKey.Key)) {
				throw new ConfigurationKeyAttributeEmptyException(typeof(T));
			}

			return configurationKey.Key;
		}
	}
}
