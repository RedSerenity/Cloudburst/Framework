using System;
using Cloudburst.Configuration.Providers;
using Cloudburst.Consul.Models;
using Consul;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;

namespace Cloudburst.Configuration.Sources {
	public class ConsulConfigurationSource : JsonStreamConfigurationSource {
		public string Key { get; set; }
		public bool Optional { get; set; }
		public bool ReloadOnChange { get; set; }
		public TimeSpan PollWaitTime { get; set; } = TimeSpan.FromMinutes(5);

		public override IConfigurationProvider Build(IConfigurationBuilder builder) =>
			new ConsulConfigurationProvider(this);
	}
}
