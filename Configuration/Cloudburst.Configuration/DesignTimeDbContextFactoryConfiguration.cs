using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Cloudburst.Configuration {
	public class DesignTimeDbContextFactoryConfiguration {

		public static IConfiguration Configuration = new ConfigurationBuilder()
			.SetBasePath(Directory.GetCurrentDirectory())
			.AddJsonFile("dbsettings.json", optional: true, reloadOnChange: true)
			.AddJsonFile("DbSettings.json", optional: true, reloadOnChange: true)
			.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
			.AddJsonFile("AppSettings.json", optional: true, reloadOnChange: true)
			.AddEnvironmentVariables()
			.Build();

		public static string GetDesignTimeDbContextConnectionString() {
			IConfigurationSection connectionString = Configuration.GetSection("DesignTimeDatabaseConnection");
			if (connectionString is null) {
				throw new Exception("Could not get DesignTime Database Connection string.");
			}

			return connectionString.Value;
		}

	}
}
