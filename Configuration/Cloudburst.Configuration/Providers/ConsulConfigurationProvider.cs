using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Consul;
using Cloudburst.Configuration.Sources;
using Cloudburst.Consul.Models;
using Cloudburst.Extensions.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Hosting.Internal;

namespace Cloudburst.Configuration.Providers {
	public class ConsulConfigurationProvider : JsonStreamConfigurationProvider, IDisposable {
		private readonly ConsulConfigurationSource _source;

		private readonly IConsulClient _consulClient;
		private readonly CancellationTokenSource _cancellationTokenSource;

		private Task _pollingTask;
		private ulong _lastIndex;
		private bool _disposed { get; set; }

		public ConsulConfigurationProvider(ConsulConfigurationSource source) : base(source) {
			_source = source ?? throw new ArgumentNullException(nameof(source), $"{typeof(ConsulConfigurationProvider).FullName}");

			var consulOptions = LoadConfigurationFromDisk();

			_consulClient = new ConsulClient(config => {
				config.Address = new Uri(consulOptions.Address);
				config.Datacenter = consulOptions.DataCenter;
				config.Token = consulOptions.Token;
				config.WaitTime = TimeSpan.FromSeconds(consulOptions.WaitTime);
			});

			_cancellationTokenSource = new CancellationTokenSource();
		}

		private ConsulOptions LoadConfigurationFromDisk() {
			// We are basically re-creating part of WebHostBuilder here to access the Consul configuration
			// stored in the AppSettings files. I'm not sure of a better way to do this... needs research.
			var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			var configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
				.AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
				.AddJsonFileDefaults(new HostingEnvironment { EnvironmentName = environment})
				.AddEnvironmentVariables()
				.Build();

			var configSection = configuration.GetSection("Cloudburst:Consul");
			var consulOptions = configSection.Get<ConsulOptions>();

			if (consulOptions is { }) return consulOptions;

			Console.WriteLine($"{typeof(ConsulConfigurationProvider).FullName} ConsulOptions is null");
			throw new ArgumentNullException(nameof(consulOptions), $"{typeof(ConsulConfigurationProvider).FullName}");
		}

		public override void Load() {
			var cancellationToken = _cancellationTokenSource.Token;

			if (!(_pollingTask is null)) { // We are already polling, so no need to Load() again.
				return;
			}

			var loadTask = Task.Run(() => LoadAsync(false, cancellationToken), cancellationToken);
			loadTask.GetAwaiter().GetResult();

			if (_source.ReloadOnChange) {
				_pollingTask = Task.Run(() => PollForChanges(cancellationToken), cancellationToken);
			}
		}

		private async Task PollForChanges(CancellationToken cancellationToken) {
			try {
				while (!cancellationToken.IsCancellationRequested) {
					await LoadAsync(true, cancellationToken);
				}
			} catch (Exception exception) {
				// Do something here. Maybe Polly instead of Try/Catch?
				Console.WriteLine(exception.Message);
				await Task.Delay(TimeSpan.FromSeconds(15), cancellationToken);
			}
		}

		private async Task LoadAsync(bool waitForChanges, CancellationToken cancellationToken) {
			var queryOptions = new QueryOptions {
				WaitTime = _source.PollWaitTime,
				WaitIndex = waitForChanges ? _lastIndex : 0
			};

			var result = await _consulClient.KV.Get(_source.Key, queryOptions, cancellationToken);

			if (!(result.StatusCode == HttpStatusCode.OK || result.StatusCode == HttpStatusCode.NotFound)) {
				throw new Exception($"Error loading configuration from consul. Status code: {result.StatusCode}.");
			}

			var kvPair = result.Response;

			if (!kvPair.HasValue() && !_source.Optional) {
				throw new Exception($"The configuration for key {_source.Key} was not found and is not optional.");
			}

			_lastIndex = result.LastIndex;

			if (kvPair.HasValue()) {
				await using (var stream = new MemoryStream(kvPair.Value)) {
					base.Load(stream);
				}

				if (waitForChanges) {
					OnReload();
				}
			}
		}

		public void Dispose() {
			if (_disposed) {
				return;
			}

			_cancellationTokenSource.Cancel();
			_cancellationTokenSource.Dispose();

			_consulClient.Dispose();
			_disposed = true;
		}
	}
}
