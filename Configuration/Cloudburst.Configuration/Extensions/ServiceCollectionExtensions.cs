using Cloudburst.Configuration.Extensions.Options;
using Cloudburst.Configuration.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Configuration {
	public static class ServiceCollectionExtensions {
		public static IServiceCollection AddCloudConfigurationOptions(this IServiceCollection services, IConfiguration configuration) {
			services.BindOption<ConfigurationOptions>(configuration);
			return services;
		}
	}
}
