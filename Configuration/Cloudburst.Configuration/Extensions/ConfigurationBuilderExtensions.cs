using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Cloudburst.Configuration.Models;
using Cloudburst.Configuration.Sources;
using Cloudburst.Consul.Models;
using Cloudburst.Version.Abstractions;
using Consul;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Polly;
using Policy = Polly.Policy;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Configuration {
	public static class ConfigurationBuilderExtensions {
		public static IConfigurationBuilder AddConsul(this IConfigurationBuilder builder, Action<ConsulConfigurationSource> options = null) {
			return builder.Add(options);
		}

		public static IConfigurationBuilder AddJsonFileDefaults(this IConfigurationBuilder builder, IHostEnvironment environment) {
			builder
				.AddJsonFile("AppSettings.Local.json", true, true)
				.AddJsonFile("Settings.json", true, true);

			if (!String.IsNullOrEmpty(environment.EnvironmentName)) {
				builder.AddJsonFile($"Settings.{environment.EnvironmentName}.json", true, true);
			}

			return builder.AddJsonFile("Settings.Local.json", true, true);
		}

		public static IConfigurationBuilder AddConsulConfigurationProvider(this IConfigurationBuilder builder, IHostEnvironment environment, ConfigurationOptions options, ConsulOptions consulOptions, Type versionType, Action<ConsulConfigurationSource> consulConfigurationAction) {
			if (builder is null) {
				throw new ArgumentNullException(nameof(builder));
			}

			if (environment is null) {
				throw new ArgumentNullException(nameof(environment));
			}

			if (options is null) {
				throw new ArgumentNullException(nameof(options));
			}

			if (!options.UseConsul) {
				return builder;
			}

			var rootKey = BuildRootKey(options, environment, versionType);
			var commonKey = $"{options.ConsulOptions.RootKey}/Common";
			var appKey = $"{rootKey}/AppSettings";

			builder
				.AddConsul(config =>
					config.BuildConsulConfig(commonKey, options, consulConfigurationAction))
				.AddConsul(config =>
					config.BuildConsulConfig(appKey, options, consulConfigurationAction));

			InitMissingConsulKeys(
				consulOptions.Address,
				consulOptions.DataCenter,
				new[] {commonKey, appKey}
			);

			return builder;
		}

		public static void BuildConsulConfig(this ConsulConfigurationSource consulOptions, string key, ConfigurationOptions options, Action<ConsulConfigurationSource> consulConfigurationAction) {
			consulOptions.Optional = true;
			consulOptions.ReloadOnChange = true;
			consulOptions.PollWaitTime = TimeSpan.FromSeconds(options.ConsulOptions.PollWaitTime);
			consulOptions.Key = key;

			// Allow dev to override default config
			consulConfigurationAction?.Invoke(consulOptions);
		}

		public static string BuildRootKey(ConfigurationOptions options, IHostEnvironment environment, Type versionType) {
			var keyStringBuilder = new StringBuilder();

			var cleanRootKey = options.ConsulOptions.RootKey.TrimEnd('/');
			keyStringBuilder.Append(cleanRootKey);

			if (options.ConsulOptions.UseVersioning) {
				if (versionType is null) {
					throw new Exception("Configuration.ConsulOptions.UseVersioning was true, but no IServiceVersion class was specified.");
				}

				IServiceVersion serviceVersion = (IServiceVersion) Activator.CreateInstance(versionType);
				if (serviceVersion is null) {
					throw new Exception("No instance of IServiceVersion found. Could not add versioning to ConsulConfiguration Provider.");
				}

				keyStringBuilder.Append($"/{serviceVersion}");
			}

			if (options.ConsulOptions.UseEnvironment) {
				keyStringBuilder.Append($"/{environment.EnvironmentName}");
			}

			Console.WriteLine($"Configuration Consul Path is {keyStringBuilder}");
			return keyStringBuilder.ToString();
		}

		private static void InitMissingConsulKeys(string consulHost, string dataCenter, string[] keys) {
			var retryPolicy = Policy
				.Handle<Exception>()
				.WaitAndRetryForever(
					retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
					(exception, timeSpan) => {
						Console.WriteLine($"[Consul Exception] {exception.Message}\n[Consul Retry Attempt] Next attempt in {timeSpan.TotalSeconds} seconds.\n [Consul Host] {consulHost} / {dataCenter}\n[Consul Keys] {String.Join(',', keys)}");
					}
				);

			retryPolicy.Execute(() => {
				var consulClient = new ConsulClient(consulConfig => {
					consulConfig.Address = new Uri(consulHost);
					consulConfig.Datacenter = dataCenter;
				});

				foreach (string key in keys) {
					QueryResult<KVPair> value = consulClient.KV.Get(key).GetAwaiter().GetResult();

					if (value.StatusCode == HttpStatusCode.NotFound) {
						consulClient.KV.Put(
							new KVPair(key) {
								Value = Encoding.UTF8.GetBytes("{}")
							}).GetAwaiter().GetResult();
					}
				}
			});
		}
	}
}
