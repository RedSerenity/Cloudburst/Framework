using System;
using Cloudburst.Configuration.Extensions.Options;
using Cloudburst.Configuration.Models;
using Cloudburst.Configuration.Sources;
using Cloudburst.Consul.Models;
using Cloudburst.Version.Abstractions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Configuration {

	public static class HostBuilderExtensions {
		public static IHostBuilder UseCloudConfiguration(this IHostBuilder builder) {
			return builder.UseCloudConfiguration(null, null);
		}

		public static IHostBuilder UseCloudConfiguration<TServiceVersion>(this IHostBuilder builder) where TServiceVersion : class, IServiceVersion {
			return builder.UseCloudConfiguration(typeof(TServiceVersion), null);
		}

		public static IHostBuilder UseCloudConfiguration<TServiceVersion>(this IHostBuilder builder, Action<ConsulConfigurationSource> consulConfigurationAction) where TServiceVersion : class, IServiceVersion {
			return builder.UseCloudConfiguration(typeof(TServiceVersion), consulConfigurationAction);
		}

		public static IHostBuilder UseCloudConfiguration(this IHostBuilder builder, Type versionType, Action<ConsulConfigurationSource> consulConfigurationAction) {
			if (!(versionType is null) && !typeof(IServiceVersion).IsAssignableFrom(versionType)) {
				throw new ArgumentOutOfRangeException($"{nameof(versionType)} must inherit from IServiceVersion");
			}

			// Should this be AppConfiguration or HostConfiguration or both?
			builder.ConfigureAppConfiguration((builderContext, config) => {
				config.AddJsonFileDefaults(builderContext.HostingEnvironment);
			});

			builder.ConfigureAppConfiguration((builderContext, config) => {
				var currentConfig = config.Build();
				var configurationOptions = currentConfig.GetWithKey<ConfigurationOptions>();
				var consulOptions = currentConfig.GetWithKey<ConsulOptions>();

				config.AddConsulConfigurationProvider(builderContext.HostingEnvironment, configurationOptions, consulOptions, versionType, consulConfigurationAction);
				config.AddEnvironmentVariables();
			});

			builder.ConfigureServices((builderContext, services) => {
				services.AddCloudConfigurationOptions(builderContext.Configuration);
			});

			return builder;
		}
	}
}
