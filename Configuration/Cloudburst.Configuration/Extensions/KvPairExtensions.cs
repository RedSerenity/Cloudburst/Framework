using System.Linq;
using Consul;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.Configuration {
	public static class KvPairExtensions {
		public static bool HasValue(this KVPair kvPair) {
			return !(kvPair is null) && !kvPair.Key.EndsWith("/") && kvPair.Value != null && kvPair.Value.Any();
		}
	}
}
