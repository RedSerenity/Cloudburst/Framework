using System;

namespace Cloudburst.Configuration.Attributes {
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public class ConfigurationKeyAttribute : Attribute {
		public readonly string Key;

		public ConfigurationKeyAttribute(string key) {
			Key = key;
		}
	}
}
