using System;

namespace Cloudburst.Configuration.Exceptions {
	public class ConfigurationKeyAttributeEmptyException : Exception {
		public ConfigurationKeyAttributeEmptyException(Type type) : base($"[ConfigurationKey()] on {type.FullName} cannot be null or empty.") { }
		public ConfigurationKeyAttributeEmptyException(Type type, Exception innerException) : base($"[ConfigurationKey()] on {type.FullName} cannot be null or empty.", innerException) { }
	}
}
