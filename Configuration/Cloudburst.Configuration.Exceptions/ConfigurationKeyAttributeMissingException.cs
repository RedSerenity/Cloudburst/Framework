using System;

namespace Cloudburst.Configuration.Exceptions {
	public class ConfigurationKeyAttributeMissingException : Exception {
		public ConfigurationKeyAttributeMissingException(Type type) : base($"{type.FullName} is missing required attribute [ConfigurationKey(\"\")]") { }
		public ConfigurationKeyAttributeMissingException(Type type, Exception innerException) : base($"{type.FullName} is missing required attribute [ConfigurationKey(\"\")]", innerException) { }
	}
}
