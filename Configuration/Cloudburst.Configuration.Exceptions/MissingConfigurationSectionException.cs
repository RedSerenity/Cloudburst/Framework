﻿using System;

namespace Cloudburst.Configuration.Exceptions {
	public class MissingConfigurationSectionException : Exception {
		public MissingConfigurationSectionException(string configurationKey, Type type) : base($"Missing configuration section {configurationKey}. Unable to create typed option {type.FullName}") { }
		public MissingConfigurationSectionException(string configurationKey, Type type, Exception innerException) : base($"Missing configuration section {configurationKey}. Unable to create typed option {type.FullName}", innerException) { }
	}
}
