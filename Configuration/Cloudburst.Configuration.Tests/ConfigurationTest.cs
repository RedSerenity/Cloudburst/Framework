using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Cloudburst.Configuration.Models;
using Cloudburst.Configuration.Extensions.Options;
using Cloudburst.Consul.Models;
using Cloudburst.Extensions.Configuration;
using Consul;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting.Internal;
using Xunit;

namespace Cloudburst.Configuration.Tests {
	public class ConfigurationTest {
		private readonly IConfigurationRoot _configuration;
		private readonly ConfigurationOptions _configurationOptions;
		private readonly ConsulOptions _consulOptions;
		private readonly HostingEnvironment _environment;

		public ConfigurationTest() {
			_environment = new HostingEnvironment {
				EnvironmentName = "Development"
			};

			var currentDir = Directory.GetCurrentDirectory();
			var configurationBuilder = new ConfigurationBuilder()
				.SetBasePath(currentDir);
			configurationBuilder.AddJsonFileDefaults(_environment);

			var currentConfig = configurationBuilder.Build();
			_configurationOptions = currentConfig.GetWithKey<ConfigurationOptions>();
			_consulOptions = currentConfig.GetWithKey<ConsulOptions>();

			configurationBuilder.AddConsulConfigurationProvider(_environment, _configurationOptions, _consulOptions, null, null);

			_configuration = configurationBuilder.Build();

			var createTask = Task.Run(() => CreateConfig());
			createTask.GetAwaiter().GetResult();
		}

		private async Task CreateConfig() {
			var consulClient = new ConsulClient(config => {
				config.Address = new Uri(_consulOptions.Address);
				config.Datacenter = _consulOptions.DataCenter;
			});

			var rootKey = _configurationOptions.ConsulOptions.RootKey;
			var commonKey = $"{rootKey}/Common";
			var appRootKey = ConfigurationBuilderExtensions.BuildRootKey(_configurationOptions, _environment, null);
			var appSettingsKey = $"{appRootKey}/AppSettings";

			await consulClient.KV.Put(new KVPair(commonKey) {
				Value = Encoding.UTF8.GetBytes("{\"MyApp\":{\"CommonSection\":{\"TrueOrFalse\":true}}}")
			});

			await consulClient.KV.Put(new KVPair(appSettingsKey) {
				Value = Encoding.UTF8.GetBytes("{\"MyApp\":{\"ConnectionString\":\"Hello, World\"}}")
			});
		}

		[Fact]
		public void ConfigurationProvider() {
			var config = _configuration.GetSection("MyApp");
			var one = config.GetValue<bool>("CommonSection:TrueOrFalse");
			var two = config.GetValue<string>("ConnectionString");

			Assert.True(one);
			Assert.Equal("Hello, World", two);
		}

		[Fact]
		public void GetCommonConfig() {
			var options = new MyAppOptions();

			Assert.Null(options.ConnectionString);
			Assert.Null(options.CommonSection);

			_configuration.Bind("MyApp", options);

			Assert.Equal("Hello, World", options.ConnectionString);
			Assert.NotNull(options.CommonSection);
			Assert.True(options.CommonSection.TrueOrFalse);
		}

		[Fact]
		public void GetAppSettingsConfig() {
			var options = new MyAppOptions();

			Assert.Null(options.ConnectionString);
			Assert.Null(options.CommonSection);

			_configuration.Bind("MyApp", options);

			Assert.Equal("Hello, World", options.ConnectionString);
			Assert.NotNull(options.CommonSection);
			Assert.True(options.CommonSection.TrueOrFalse);
		}

		[Fact]
		public async Task TestLongPolling() {
			var consulClient = new ConsulClient(config => {
				config.Address = new Uri(_consulOptions.Address);
				config.Datacenter = _consulOptions.DataCenter;
			});

			var appRootKey = ConfigurationBuilderExtensions.BuildRootKey(_configurationOptions, _environment, null);
			var appSettingsKey = $"{appRootKey}/AppSettings";

			var config = _configuration.GetSection("MyApp");
			var result = config.GetValue<string>("ConnectionString");

			Assert.Equal("Hello, World", result);

			await consulClient.KV.Put(new KVPair(appSettingsKey) {
				Value = Encoding.UTF8.GetBytes("{\"MyApp\":{\"ConnectionString\":\"Hello, World!\"}}")
			});

			await Task.Delay(TimeSpan.FromSeconds(10));

			result = config.GetValue<string>("ConnectionString");

			Assert.Equal("Hello, World!", result);
		}
	}
}
