namespace Cloudburst.Configuration.Tests {
	public class MyAppOptions {
		public string ConnectionString { get; set; }
		public CommonSectionOptions CommonSection { get; set; }
	}
}
