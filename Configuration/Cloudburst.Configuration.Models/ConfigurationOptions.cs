﻿using Cloudburst.Configuration.Attributes;

namespace Cloudburst.Configuration.Models {
	[ConfigurationKey("Cloudburst:Configuration")]
	public class ConfigurationOptions {
		public bool UseConsul { get; set; } = true;
		public ConsulProviderOptions ConsulOptions { get; set; } = new ConsulProviderOptions();
	}
}
