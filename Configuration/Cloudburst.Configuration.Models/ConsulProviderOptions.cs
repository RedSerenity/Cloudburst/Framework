using Cloudburst.Configuration.Attributes;

namespace Cloudburst.Configuration.Models {
	[ConfigurationKey("Cloudburst:Configuration:ConsulOptions")]
	public class ConsulProviderOptions {
		public string RootKey { get; set; } = "AppRoot";
		public int PollWaitTime { get; set; } = 3;
		public bool UseEnvironment { get; set; } = true;
		public bool UseVersioning { get; set; } = false;
	}
}
