using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Cloudburst.HealthChecks.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Cloudburst.HealthChecks {
	public class ResponseWriter {
		public static Task NoContentWriteResponse(HttpContext httpContext, HealthReport report) {
			httpContext.Response.ContentType = "application/json; charset=utf-8";

			switch (report.Status) {
				case HealthStatus.Unhealthy:
					httpContext.Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
					break;
				case HealthStatus.Degraded:
					httpContext.Response.StatusCode = StatusCodes.Status424FailedDependency;
					break;
				case HealthStatus.Healthy:
					httpContext.Response.StatusCode = StatusCodes.Status200OK;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			return httpContext.Response.WriteAsync(String.Empty);
		}

		public static Task WriteResponse(HttpContext httpContext, HealthReport report) {
			httpContext.Response.ContentType = "application/json; charset=utf-8";

			switch (report.Status) {
				case HealthStatus.Unhealthy:
					httpContext.Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
					break;
				case HealthStatus.Degraded:
					httpContext.Response.StatusCode = StatusCodes.Status424FailedDependency;
					break;
				case HealthStatus.Healthy:
					httpContext.Response.StatusCode = StatusCodes.Status200OK;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			var options = new JsonSerializerOptions {
				Converters = { new JsonStringEnumConverter() },
				WriteIndented = true
			};

			var isDetailed = httpContext.Request.Query.ContainsKey("detailed");
			var isSimple = httpContext.Request.Query.ContainsKey("simple");

			var response = new HealthCheckResponse {
				Status = report.Status,
				TotalDuration = (int) report.TotalDuration.TotalMilliseconds,
				Results = isSimple ? new List<HealthCheckEntry>() :
					report.Entries.Select(x => new HealthCheckEntry {
						Name = x.Key,
						Status = x.Value.Status,
						Description = x.Value.Description,
						Duration = (int) x.Value.Duration.TotalMilliseconds,
						Data = x.Value.Data,
						Tags = x.Value.Tags,
						Error = x.Value.Exception?.Message,
						Exception = isDetailed ? x.Value.Exception?.ToString() : null,
						StackTrace = isDetailed ? x.Value.Exception?.StackTrace : null
					}).ToList()
			};

			string serializedResponse = JsonSerializer.Serialize(response, options);
			return httpContext.Response.WriteAsync(serializedResponse);
		}
	}
}
