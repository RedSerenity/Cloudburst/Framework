using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.HealthChecks {
	public static class HostBuilderExtensions {
		public static IHostBuilder UseCloudHealthCheck(this IHostBuilder builder) {
			return builder.UseCloudHealthCheck(healthChecksBuilder => { });
		}
		public static IHostBuilder UseCloudHealthCheck(this IHostBuilder builder, Action<IHealthChecksBuilder> healthChecksBuilder) {
			builder.ConfigureServices((builderContext, services) => {
				services.AddCloudHealthCheck(healthChecksBuilder);
			});

			return builder;
		}
	}
}
