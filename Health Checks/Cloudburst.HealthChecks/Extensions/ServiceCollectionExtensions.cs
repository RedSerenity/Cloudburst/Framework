using System;
using Cloudburst.HealthChecks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.HealthChecks {
	public static class ServiceCollectionExtensions {
		public static IServiceCollection AddCloudHealthCheck(this IServiceCollection services) {
			return services.AddCloudHealthCheck(healthChecksBuilder => { });
		}
		public static IServiceCollection AddCloudHealthCheck(this IServiceCollection services, Action<IHealthChecksBuilder> healthChecksBuilder) {
			var builder = services.AddHealthChecks()
				.AddCheck<ServiceRunningHealthCheck>(
					"Cloudburst.ServiceRunning.HealthCheck",
					HealthStatus.Unhealthy
				);

			healthChecksBuilder?.Invoke(builder);

			return services;
		}
	}
}
