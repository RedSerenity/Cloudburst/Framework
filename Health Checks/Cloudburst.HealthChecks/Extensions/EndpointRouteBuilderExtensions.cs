﻿using System;
using Cloudburst.HealthChecks;
using Cloudburst.Version.Abstractions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.HealthChecks {
	public static class EndpointRouteBuilderExtensions {
		public static IEndpointRouteBuilder MapCloudHealthChecks(this IEndpointRouteBuilder routes) {
			return routes.MapCloudHealthChecks(options => { });
		}

		public static IEndpointRouteBuilder MapCloudHealthChecks(this IEndpointRouteBuilder routes, Action<HealthCheckOptions> options) {
			var healthCheckOptions = new HealthCheckOptions {
				ResponseWriter = ResponseWriter.WriteResponse
			};

			options?.Invoke(healthCheckOptions);

			var version = routes.ServiceProvider.GetService<IServiceVersion>();

			routes.MapHealthChecks("/health", healthCheckOptions);

			// Used by Cloudburst.ServiceRegistration to ensure that a specific instance of the containerized app is healthy
			var noContentHealthCheckOptions = new HealthCheckOptions {
				ResponseWriter = ResponseWriter.NoContentWriteResponse
			};
			routes.MapHealthChecks($"/health/{version.UniqueInstanceIdentifier}", noContentHealthCheckOptions);
			return routes;
		}

		public static IEndpointRouteBuilder MapCloudReadyLiveHealthChecks(this IEndpointRouteBuilder routes) {
			return routes
				.MapCloudLiveHealthChecks(liveOptions => { })
				.MapCloudReadyHealthChecks(readyOptions => { });
		}

		public static IEndpointRouteBuilder MapCloudReadyLiveHealthChecks(this IEndpointRouteBuilder routes, Action<HealthCheckOptions> readyOptions, Action<HealthCheckOptions> liveOptions) {
			return routes
				.MapCloudLiveHealthChecks(liveOptions)
				.MapCloudReadyHealthChecks(readyOptions);
		}

		public static IEndpointRouteBuilder MapCloudReadyHealthChecks(this IEndpointRouteBuilder routes) {
			return routes.MapCloudReadyHealthChecks(readyOptions => { });
		}

		public static IEndpointRouteBuilder MapCloudReadyHealthChecks(this IEndpointRouteBuilder routes, Action<HealthCheckOptions> readyOptions) {
			var readyHealthCheckOptions = new HealthCheckOptions {
				ResponseWriter = ResponseWriter.WriteResponse,
				Predicate = check => check.Tags.Contains("ready")
			};
			readyOptions?.Invoke(readyHealthCheckOptions);

			var version = routes.ServiceProvider.GetService<IServiceVersion>();

			routes.MapHealthChecks("/health/ready", readyHealthCheckOptions);

			// Used by Cloudburst.ServiceRegistration to ensure that a specific instance of the containerized app is healthy
			var noContentHealthCheckOptions = new HealthCheckOptions {
				ResponseWriter = ResponseWriter.NoContentWriteResponse,
				Predicate = check => check.Tags.Contains("ready")
			};
			routes.MapHealthChecks($"/health/ready/{version.UniqueInstanceIdentifier}", noContentHealthCheckOptions);
			return routes;
		}

		public static IEndpointRouteBuilder MapCloudLiveHealthChecks(this IEndpointRouteBuilder routes) {
			return routes.MapCloudLiveHealthChecks(liveOptions => { });
		}

		public static IEndpointRouteBuilder MapCloudLiveHealthChecks(this IEndpointRouteBuilder routes, Action<HealthCheckOptions> liveOptions) {
			var liveHealthCheckOptions = new HealthCheckOptions {
				ResponseWriter = ResponseWriter.WriteResponse,
				Predicate = check => !check.Tags.Contains("ready")
			};
			liveOptions?.Invoke(liveHealthCheckOptions);

			var version = routes.ServiceProvider.GetService<IServiceVersion>();

			routes.MapHealthChecks("/health/live", liveHealthCheckOptions);

			// Used by Cloudburst.ServiceRegistration to ensure that a specific instance of the containerized app is healthy
			var noContentHealthCheckOptions = new HealthCheckOptions {
				ResponseWriter = ResponseWriter.NoContentWriteResponse,
				Predicate = check => !check.Tags.Contains("ready")
			};
			routes.MapHealthChecks($"/health/live/{version.UniqueInstanceIdentifier}", noContentHealthCheckOptions);
			return routes;
		}
	}
}
