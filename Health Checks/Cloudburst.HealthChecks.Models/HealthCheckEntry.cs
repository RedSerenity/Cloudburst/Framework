using System.Collections.Generic;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Cloudburst.HealthChecks.Models {
	public class HealthCheckEntry {
		public string Name { get; set; }
		public HealthStatus Status { get; set; }
		public string Description { get; set; }
		public int Duration { get; set; }
		public IReadOnlyDictionary<string, object> Data { get; set; }
		public IEnumerable<string> Tags { get; set; }
		public string Error { get; set; }
		public string Exception { get; set; }
		public string StackTrace { get; set; }
	}
}
