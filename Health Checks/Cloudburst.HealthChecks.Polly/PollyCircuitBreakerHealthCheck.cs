using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Polly.CircuitBreaker;

namespace Cloudburst.HealthChecks.Polly {
	/*
	   services.AddPollyCircuitBreakerHealthCheck();

	   services.AddHealthCheck()
	     .AddCheck<PollyCircuitBreakerHealthCheck>(
					"Polly.CircuitBreaker.HealthCheck",
					HealthStatus.Unhealthy
				);
	 */
	public class PollyCircuitBreakerHealthCheck : IHealthCheck {
		private readonly IEnumerable<ICircuitBreakerPolicy> _circuitBreakerPolicies;
		private readonly IReadOnlyDictionary<string, object> _healthData;

		public PollyCircuitBreakerHealthCheck(IEnumerable<ICircuitBreakerPolicy> circuitBreakerPolicies) {
			_circuitBreakerPolicies = circuitBreakerPolicies;

			_healthData = new Dictionary<string, object> {
				{
					"CircuitBreakerPolicies",
					_circuitBreakerPolicies.Select(x => x.GetType().FullName).ToArray()
				}
			};
		}

		public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken()) {
			var badCircuitList = new List<string>();

			foreach (var policy in _circuitBreakerPolicies) {
				if (policy.CircuitState != CircuitState.Closed) {
					badCircuitList.Add(policy.GetType().FullName);
				}
			}

			if (badCircuitList.Any()) {
				var data = new Dictionary<string, object> {
					{
						"TrippedCircuits", badCircuitList.ToArray()
					},
					{
						"ClosedCircuits", _circuitBreakerPolicies.Where(x => badCircuitList.All(y => y != x.GetType().FullName))
					}
				};
				return Task.FromResult(HealthCheckResult.Degraded("Circuit has been tripped.", null, data));
			}

			return Task.FromResult(HealthCheckResult.Healthy("No circuits are tripped.", _healthData));
		}
	}
}
