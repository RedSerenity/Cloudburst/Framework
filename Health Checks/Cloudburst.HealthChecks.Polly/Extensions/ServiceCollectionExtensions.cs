using System;
using Cloudburst.HealthChecks.Polly;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

// ReSharper disable once CheckNamespace
namespace Cloudburst.Extensions.HealthChecks {
	public static class ServiceCollectionExtensions {
		public static IServiceCollection AddPollyCircuitBreakerHealthCheck(this IServiceCollection services) {
			return services.AddPollyCircuitBreakerHealthCheck(healthChecksBuilder => { });
		}

		public static IServiceCollection AddPollyCircuitBreakerHealthCheck(this IServiceCollection services, Action<IHealthChecksBuilder> healthChecksBuilder) {
			var builder = services.AddHealthChecks()
				.AddCheck<PollyCircuitBreakerHealthCheck>(
					"Polly.CircuitBreaker.HealthCheck",
					HealthStatus.Unhealthy,
					new []{ "ready" }
				);

			healthChecksBuilder?.Invoke(builder);

			return services;
		}
	}
}
